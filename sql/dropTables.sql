USE training_unit;

DROP TABLE Class;
DROP TABLE InstrCourse;
DROP TABLE Instructor;
DROP TABLE StudCourse;
DROP TABLE Student;
DROP TABLE Course;
DROP TABLE Company;
