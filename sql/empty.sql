USE training_unit;

DELETE FROM Class;
DELETE FROM InstrCourse;
DELETE FROM Instructor;
DELETE FROM StudCourse;
DELETE FROM Student;
DELETE FROM Course;
DELETE FROM Company;
