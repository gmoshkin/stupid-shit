USE training_unit;

-----------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE Student
(
	id int IDENTITY(1, 1) NOT NULL,
	firstName nvarchar(20) NOT NULL,
	lastName nvarchar(20) NULL,
	middleName nvarchar(20) NULL,
	login varchar(20) NOT NULL,
	password varchar(50) NOT NULL,
	CONSTRAINT PK_STUDENT PRIMARY KEY (id)
);

CREATE TABLE Company
(
	id int IDENTITY(1, 1) NOT NULL,
	name nvarchar(100) NOT NULL,
	address nvarchar(max) NOT NULL,
	login varchar(20) NOT NULL, -- for company administrators
	password varchar(50) NOT NULL,
	CONSTRAINT PK_COMPANY PRIMARY KEY (id)
);

CREATE TABLE Instructor
(
	id int IDENTITY(1, 1) NOT NULL,
	firstName nvarchar(20) NOT NULL,
	lastName nvarchar(20) NULL,
	middleName nvarchar(20) NULL,
	login varchar(20) NOT NULL,
	password varchar(50) NOT NULL,
	companyId int NOT NULL,
	CONSTRAINT PK_INSTRUCTOR PRIMARY KEY (id)
);

CREATE TABLE Course
(
	id int IDENTITY(1, 1) NOT NULL,
	name nvarchar(100) NOT NULL,
	description nvarchar(400) NOT NULL,
	companyId int NOT NULL,
	startDate datetime NOT NULL,
	duration int NOT NULL, -- hours (total)
	CONSTRAINT PK_COURSE PRIMARY KEY (id)
);

CREATE TABLE StudCourse
(
	id int IDENTITY(1, 1) NOT NULL,
	studentId int NOT NULL,
	courseId int NOT NULL,
	CONSTRAINT PK_STUDCOURSE PRIMARY KEY (id)
);


CREATE TABLE InstrCourse
(
	id int IDENTITY(1, 1) NOT NULL,
	instructorId int NOT NULL,
	courseId int NOT NULL,
	CONSTRAINT PK_INSTRCOURSE PRIMARY KEY (id)
)

CREATE TABLE Class
(
	id int IDENTITY (1, 1) NOT NULL,
	courseId int NOT NULL,
	duration int NOT NULL, -- hours (per class)
	date datetime NOT NULL,
	CONSTRAINT PK_CLASS PRIMARY KEY (id)
);

-----------------------------------------------------------------------------------------------------------------------------------

CREATE UNIQUE INDEX IX_COURSE ON Course(name ASC);

CREATE UNIQUE INDEX IX_COMPANY ON Company(name ASC);

CREATE UNIQUE INDEX IX_STUDLOGIN ON Student(login ASC);

CREATE UNIQUE INDEX IX_INSTRLOGIN ON Instructor(login ASC);

CREATE UNIQUE INDEX IX_COMPLOGIN ON Company(login ASC);

CREATE UNIQUE INDEX IX_STUDPSWD ON Student(password ASC);

CREATE UNIQUE INDEX IX_INSTRPSWD ON Instructor(password ASC);

CREATE UNIQUE INDEX IX_COMPPSWD ON Company(password ASC);

-----------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE Instructor
	ADD CONSTRAINT FK_INSTRUCTOR_COMPANY FOREIGN KEY (companyId)
		REFERENCES Company (id);

ALTER TABLE Course
	ADD CONSTRAINT FK_COURSE_COMPANY FOREIGN KEY (companyId)
		REFERENCES Company (id);

ALTER TABLE Class
	ADD CONSTRAINT FK_CLASS_COURSE FOREIGN KEY (courseId)
		REFERENCES Course (id);

ALTER TABLE StudCourse
	ADD CONSTRAINT FK_STUDCOURSE_COURSE FOREIGN KEY (courseId)
		REFERENCES Course (id);

ALTER TABLE StudCourse
	ADD CONSTRAINT FK_STUDCOURSE_STUDENT FOREIGN KEY (studentId)
		REFERENCES Student (id);

ALTER TABLE InstrCourse
	ADD CONSTRAINT FK_INSTRCOURSE_COURSE FOREIGN KEY (courseId)
		REFERENCES Course (id);

ALTER TABLE InstrCourse
	ADD CONSTRAINT FK_INSTRCOURSE_INSTRUCTOR FOREIGN KEY (instructorId)
		REFERENCES Instructor (id);

-----------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE Class
	ADD CONSTRAINT CHK_CLASS_DATE
		CHECK (date >= GETDATE());

-----------------------------------------------------------------------------------------------------------------------------------

CREATE FUNCTION dbo.IfBelongToSameCompany(@instructorId int, @courseId int)
RETURNS int
AS
BEGIN
	DECLARE @RetVal int
	If (SELECT companyId FROM Instructor WHERE id = @instructorId) = (SELECT companyId FROM Course WHERE id = @courseId)
		SET @RetVal = 1
	ELSE
		SET @RetVal = 0
	RETURN @RetVal 
END;

ALTER TABLE InstrCourse
	ADD CONSTRAINT CHK_INSTRCOURSE_COMPANY
		CHECK (dbo.IfBelongToSameCompany(instructorId, courseId) = 1);

-----------------------------------------------------------------------------------------------------------------------------------

EXEC msdb.dbo.sp_delete_job
    @job_name = N'Daily Expired Classes Removal';
	
EXEC msdb.dbo.sp_add_job
    @job_name = N'Daily Expired Classes Removal';

-----------------------------------------------------------------------------------------------------------------------------------

EXEC msdb.dbo.sp_delete_jobstep
    @job_name = N'Daily Expired Classes Removal',
	@step_id = 0;
	
EXEC msdb.dbo.sp_add_jobstep
    @job_name = N'Daily Expired Classes Removal',
	@step_id = 1,
    @step_name = N'Remove expired classes',
    @subsystem = N'TSQL',
    @command = N'DELETE FROM Class WHERE date < GETDATE()', 
    @retry_attempts = 5,
    @retry_interval = 5;

-----------------------------------------------------------------------------------------------------------------------------------

EXEC msdb.dbo.sp_add_schedule
    @schedule_name = N'Start Of Every Day',
    @freq_type = 4,
    @freq_interval = 1;

EXEC msdb.dbo.sp_attach_schedule
   @job_name = N'Daily Expired Classes Removal',
   @schedule_name = N'Start Of Every Day';

-----------------------------------------------------------------------------------------------------------------------------------

EXEC msdb.dbo.sp_add_jobserver
    @job_name = N'Daily Expired Classes Removal';
