package org.sher.trainingunit;

import org.sher.trainingunit.dao.implementations.ClientImpl;
import org.sher.trainingunit.dao.implementations.OfficeImpl;
import org.sher.trainingunit.dao.interfaces.ClientInterface;
import org.sher.trainingunit.dto.Client;
import org.sher.trainingunit.dto.Office;
import org.testng.annotations.Test;


import java.util.Calendar;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by Arseny on 31.08.2014.
 */
public class ClientTest {
    ClientInterface ci = new ClientImpl();
    Office office = new OfficeImpl().getById(1);
    Client client = null;

    @Test
    public void save() {
        client = new Client(4529445, "Innokentiy Ivanov", Calendar.getInstance(), office, true);
        ci.save(client);
        Client inserted = ci.getById(client.getID());
        assertEquals(client, inserted);
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        int newDocument = 43555322;
        client.setDocument(newDocument);
        ci.update(client);
        Client updated = ci.getById(client.getID());
        assertEquals(newDocument, updated.getDocument());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        int id = client.getID();
        ci.delete(client);
        Client deleted = ci.getById(id);
        assertNull(deleted);
    }
}
