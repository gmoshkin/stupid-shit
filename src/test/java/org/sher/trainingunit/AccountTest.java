package org.sher.trainingunit;

import org.sher.trainingunit.dao.implementations.AccountImpl;
import org.sher.trainingunit.dao.implementations.AccountTypeImpl;
import org.sher.trainingunit.dao.implementations.ClientImpl;
import org.sher.trainingunit.dao.interfaces.AccountInterface;
import org.sher.trainingunit.dto.Account;
import org.sher.trainingunit.dto.AccountType;
import org.sher.trainingunit.dto.Client;
import org.testng.annotations.Test;


import java.util.Calendar;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by Arseny on 31.08.2014.
 */
public class AccountTest {
    AccountInterface ci = new AccountImpl();
    AccountType accountType = new AccountTypeImpl().getById(1);
    Client client = new ClientImpl().getById(1);
    Account account = null;

    @Test
    public void save() {
        account = new Account(Calendar.getInstance(), client, 500, accountType);
        ci.save(account);
        Account inserted = ci.getById(account.getNumber());
        assertEquals(account, inserted);
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        int newBalance = 1000;
        account.setBalance(newBalance);
        ci.update(account);
        Account updated = ci.getById(account.getNumber());
        assertEquals(newBalance, updated.getBalance());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        int id = account.getNumber();
        ci.delete(account);
        Account deleted = ci.getById(id);
        assertNull(deleted);
    }
}
