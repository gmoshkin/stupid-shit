package org.sher.trainingunit;

import org.sher.trainingunit.dao.implementations.AddressImpl;
import org.sher.trainingunit.dao.implementations.ClientImpl;
import org.sher.trainingunit.dao.interfaces.AddressInterface;
import org.sher.trainingunit.dto.Address;
import org.sher.trainingunit.dto.Client;
import org.testng.annotations.Test;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by Arseny on 31.08.2014.
 */
public class AddressTest {
    AddressInterface ai = new AddressImpl();
    Client client = new ClientImpl().getById(1);
    Address address = null;

    @Test
    public void save() {
        address = new Address("Novoyasenevskaya, 2/10", client);
        ai.save(address);
        Address inserted = ai.getById(address.getAddress());
        assertEquals(address, inserted);
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        Client newClient = new ClientImpl().getById(2);
        address.setClient(newClient);
        ai.update(address);
        Address updated = ai.getById(address.getAddress());
        assertEquals(newClient, updated.getClient());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        String ad = address.getAddress();
        ai.delete(address);
        Address deleted = ai.getById(ad);
        assertNull(deleted);
    }
}
