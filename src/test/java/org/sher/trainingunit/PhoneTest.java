package org.sher.trainingunit;

import org.sher.trainingunit.dao.implementations.PhoneImpl;
import org.sher.trainingunit.dao.implementations.ClientImpl;
import org.sher.trainingunit.dao.interfaces.PhoneInterface;
import org.sher.trainingunit.dto.Phone;
import org.sher.trainingunit.dto.Client;
import org.testng.annotations.Test;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Created by Arseny on 31.08.2014.
 */
public class PhoneTest {
    PhoneInterface ai = new PhoneImpl();
    Client client = new ClientImpl().getById(1);
    Phone phone = null;

    @Test
    public void save() {
        phone = new Phone("89063837636", client);
        ai.save(phone);
        Phone inserted = ai.getById(phone.getPhone());
        assertEquals(phone, inserted);
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        Client newClient = new ClientImpl().getById(2);
        phone.setClient(newClient);
        ai.update(phone);
        Phone updated = ai.getById(phone.getPhone());
        assertEquals(newClient, updated.getClient());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        String ad = phone.getPhone();
        ai.delete(phone);
        Phone deleted = ai.getById(ad);
        assertNull(deleted);
    }
}
