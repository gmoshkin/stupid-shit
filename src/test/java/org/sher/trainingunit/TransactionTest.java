package org.sher.trainingunit;

import org.sher.trainingunit.dao.implementations.TransactionImpl;
import org.sher.trainingunit.dao.implementations.AccountImpl;
import org.sher.trainingunit.dao.interfaces.TransactionInterface;
import org.sher.trainingunit.dto.Transaction;
import org.sher.trainingunit.dto.Account;
import org.testng.annotations.Test;


import java.util.Calendar;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 * Created by Arseny on 31.08.2014.
 */
public class TransactionTest {
    TransactionInterface ci = new TransactionImpl();
    Account account = new AccountImpl().getById(1);
    Transaction transaction = null;

    @Test
    public void save() {
        transaction = new Transaction(account, 100, Calendar.getInstance(), (float) 0.1);
        ci.save(transaction);
        Transaction inserted = ci.getById(transaction.getID());
        assertTrue(transaction.equals(inserted));
    }

    @Test(dependsOnMethods = {"save"})
    public void update() {
        Account newAccount = new AccountImpl().getById(2);
        transaction.setAccount(newAccount);
        ci.update(transaction);
        Transaction updated = ci.getById(transaction.getID());
        assertEquals(newAccount, updated.getAccount());
    }

    @Test(dependsOnMethods = {"update"})
    public void delete() {
        int id = transaction.getID();
        ci.delete(transaction);
        Transaction deleted = ci.getById(id);
        assertNull(deleted);
    }


}
