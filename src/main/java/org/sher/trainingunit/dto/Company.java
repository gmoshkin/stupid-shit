package org.sher.trainingunit.dto;

import java.util.Set;

public class Company extends NamedEntity implements User {
	
	private String address;

	private EmbeddedUser user;
	
	private Set<Instructor> instructors;
	private Set<Course> courses;
	
	public Company() {
		user = new EmbeddedUser(null, null);
	}
	public Company(String name, String login, String password, String address) {
		user = new EmbeddedUser(login, password);
		this.setName(name);
        this.address = address;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLogin() {
		return user.getLogin();
	}
	public void setLogin(String login) {
		user.setLogin(login);
	}
	public String getPassword() {
		return user.getPassword();
	}
	public void setPassword(String password) {
		user.setPassword(password);
	}
	public Set<Instructor> getInstructors() {
		return instructors;
	}
	public void setInstructors(Set<Instructor> instructors) {
		this.instructors = instructors;
	}
    public boolean removeInstructor(Instructor instructor) {
        return instructors.remove(instructor);
    }
    public Set<Course> getCourses() {
		return courses;
	}
	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}
}
