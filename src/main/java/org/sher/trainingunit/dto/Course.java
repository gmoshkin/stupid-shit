package org.sher.trainingunit.dto;

import java.util.Date;
import java.util.Set;

public class Course extends NamedEntity{
	
	private String description;
	private Date startDate;
	private int duration;
	
	private Company company;
	private Set<Student> students;
	private Set<Instructor> instructors;
	private Set<Class> classes;
	
	public Course() {
		this.setName(null);
		this.description = null;
		this.startDate = new Date();
		this.duration = 0;
		this.company = null;
		this.students = null;
		this.instructors = null;
		this.classes = null;
	}
	public Course(String name, String description) {
		this.setName(name);
		this.description = description;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public Set<Student> getStudents() {
		return students;
	}
	public void setStudents(Set<Student> students) {
		this.students = students;
	}
    public boolean removeStudent(Student student) {
        return students.remove(student);
    }
	public void addStudent(Student student) {
		this.students.add(student);
	}
	public Set<Instructor> getInstructors() {
		return instructors;
	}
    public void setInstructors(Set<Instructor> instructors) {
        this.instructors = instructors;
    }
    public void addInstructor(Instructor instructor) {
        this.instructors.add(instructor);
    }
    public boolean removeInstructor(Instructor instructor) {
        return instructors.remove(instructor);
    }
    public Set<Class> getClasses() {
		return classes;
	}
    public void setClasses(Set<Class> classes) {
		this.classes = classes;
	}
    public void addClass(Class klass) {
		this.classes.add(klass);
	}
}
