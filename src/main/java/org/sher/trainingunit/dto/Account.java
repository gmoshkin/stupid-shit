package org.sher.trainingunit.dto;

import org.sher.trainingunit.dao.DBhelper;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mgn on 29.08.2014.
 */
@javax.persistence.Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int number;
//    @Temporal(TemporalType.DATE) //only date, no time and timezone
    private Date creationDate;
    @ManyToOne
    @JoinColumn(name = "ClientID")
    private Client client;
    private int balance;
    @ManyToOne
    @JoinColumn(name = "AccountTypeID")
    private AccountType accountType;
    private Date paybackDate;

    public Account(Calendar creationDate, Client client, int balance, AccountType accountType, Calendar paybackDate) {
        this.creationDate = DBhelper.normalizeCal(creationDate);
        this.client = client;
        this.balance = balance;
        this.accountType = accountType;
        this.paybackDate = DBhelper.normalizeCal(paybackDate);
    }

    public Account(Calendar creationDate, Client client, int balance, AccountType accountType) {
        this.creationDate = DBhelper.normalizeCal(creationDate);
        this.client = client;
        this.balance = balance;
        this.accountType = accountType;
    }

    public Account() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Date getPaybackDate() {
        return paybackDate;
    }

    public void setPaybackDate(Date paybackDate) {
        this.paybackDate = paybackDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (balance != account.balance) return false;
        if (number != account.number) return false;
        if (!accountType.equals(account.accountType)) return false;
        if (!client.equals(account.client)) return false;
        if (!(creationDate.compareTo(account.creationDate) == 0)) return false;
        if (paybackDate != null ? !paybackDate.equals(account.paybackDate) : account.paybackDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = number;
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + client.hashCode();
        result = 31 * result + balance;
        result = 31 * result + accountType.hashCode();
        result = 31 * result + (paybackDate != null ? paybackDate.hashCode() : 0);
        return result;
    }
}
