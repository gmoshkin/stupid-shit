package org.sher.trainingunit.dto;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by Arseny on 29.08.2014.
 */
@javax.persistence.Entity
public class Email {
    @Id
    private String email;
    @ManyToOne
    @JoinColumn(name = "ClientID")
    private Client client;

    public Email(String email, Client client) {
        this.email = email;
        this.client = client;
    }

    public Email() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Email email1 = (Email) o;

        if (!client.equals(email1.client)) return false;
        if (!email.equals(email1.email)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = email.hashCode();
        result = 31 * result + client.hashCode();
        return result;
    }
}
