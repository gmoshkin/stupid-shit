package org.sher.trainingunit.dto;

import java.util.HashSet;
import java.util.Set;

public class Instructor extends Person implements User {
	
	private EmbeddedUser user;
	
	private Company company;
	private Set<Course> courses;
	
	public Instructor() {
		user = new EmbeddedUser(null, null);
	}
	public Instructor(String fName, String mName, String lName, String login, String password) {
		user = new EmbeddedUser(login, password);
		this.setFirstName(fName);
		this.setMiddleName(mName);
		this.setLastName(lName);
	}

	public String getLogin() {
		return user.getLogin();
	}
	public void setLogin(String login) {
		user.setLogin(login);
	}
	public String getPassword() {
		return user.getPassword();
	}
	public void setPassword(String password) {
		user.setPassword(password);
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public Set<Course> getCourses() {
		return courses;
	}
	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}
	public boolean removeCourse(Course course) {
		return this.courses.remove(course);
	}
	public Set<Class> getClasses() {
		HashSet<Class> classes = new HashSet<>();
		for(Course course : courses) {
			classes.addAll(course.getClasses());
		}
		return classes;
	}
}
