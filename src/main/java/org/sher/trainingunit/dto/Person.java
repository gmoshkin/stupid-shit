package org.sher.trainingunit.dto;

public class Person extends Entity {

	private String firstName;
	private String middleName;
	private String lastName;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
    public String getFullName() {
        String fullName = this.firstName;
        String  tmp = this.middleName;
        if (tmp != null && !tmp.isEmpty())
            fullName += ' ' + tmp;
        tmp = this.lastName;
        if (tmp != null && !tmp.isEmpty())
            fullName += ' ' + tmp;
        return fullName;
    }
}
