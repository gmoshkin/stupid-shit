package org.sher.trainingunit.dto;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Class extends Entity {
	
	private int duration;
	private Date date;
	
	private Course course;
	
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
    public String getTimeInterval() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        String timeInterval;
        timeInterval = "" + calendar.get(Calendar.HOUR_OF_DAY);
        timeInterval += ":" + calendar.get(Calendar.MINUTE);
        timeInterval += " - " + (calendar.get(Calendar.HOUR_OF_DAY) + duration);
        timeInterval += ":" + calendar.get(Calendar.MINUTE);
        return timeInterval;
    }
}
