package org.sher.trainingunit.dto;

import javax.persistence.*;

/**
 * Created by Arseny on 30.08.2014.
 */

@javax.persistence.Entity
public class AccountType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;
    private int maxCredit;
    private int paybackDuration;
    private boolean isAvailableForNatural;
    private boolean isAvailableForCorporated;

    public AccountType(int maxCredit, int paybackDuration, boolean isAvailableForNatural, boolean isAvailableForCorporated) {
        this.maxCredit = maxCredit;
        this.paybackDuration = paybackDuration;
        this.isAvailableForNatural = isAvailableForNatural;
        this.isAvailableForCorporated = isAvailableForCorporated;
    }

    public AccountType() {
    }

    public AccountType(int maxCredit, boolean isAvailableForNatural, boolean isAvailableForCorporated) {
        this.maxCredit = maxCredit;
        this.isAvailableForNatural = isAvailableForNatural;
        this.isAvailableForCorporated = isAvailableForCorporated;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getMaxCredit() {
        return maxCredit;
    }

    public void setMaxCredit(int maxCredit) {
        this.maxCredit = maxCredit;
    }

    public int getPaybackDuration() {
        return paybackDuration;
    }

    public void setPaybackDuration(int paybackDuration) {
        this.paybackDuration = paybackDuration;
    }

    public boolean isAvailableForNatural() {
        return isAvailableForNatural;
    }

    public void setAvailableForNatural(boolean isAvailableForNatural) {
        this.isAvailableForNatural = isAvailableForNatural;
    }

    public boolean isAvailableForCorporated() {
        return isAvailableForCorporated;
    }

    public void setAvailableForCorporated(boolean isAvailableForCorporated) {
        this.isAvailableForCorporated = isAvailableForCorporated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountType that = (AccountType) o;

        if (ID != that.ID) return false;
        if (isAvailableForCorporated != that.isAvailableForCorporated) return false;
        if (isAvailableForNatural != that.isAvailableForNatural) return false;
        if (maxCredit != that.maxCredit) return false;
        if (paybackDuration != that.paybackDuration) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ID;
        result = 31 * result + maxCredit;
        result = 31 * result + paybackDuration;
        result = 31 * result + (isAvailableForNatural ? 1 : 0);
        result = 31 * result + (isAvailableForCorporated ? 1 : 0);
        return result;
    }
}
