package org.sher.trainingunit.dto;

public interface User {

	public String getLogin();
	public void setLogin(String login);
	public String getPassword();
	public void setPassword(String password);
}
