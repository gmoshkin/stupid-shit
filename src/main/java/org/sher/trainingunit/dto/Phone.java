package org.sher.trainingunit.dto;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by Arseny on 29.08.2014.
 */

@javax.persistence.Entity
public class Phone {
    @Id
    private String phone;
    @ManyToOne
    @JoinColumn(name = "ClientID")
    private Client client;

    public Phone(String phone, Client client) {
        this.phone = phone;
        this.client = client;
    }

    public Phone() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone1 = (Phone) o;

        if (!client.equals(phone1.client)) return false;
        if (!phone.equals(phone1.phone)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = phone.hashCode();
        result = 31 * result + client.hashCode();
        return result;
    }
}
