package org.sher.trainingunit.dto;

import org.sher.trainingunit.dao.DBhelper;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Arseny on 29.08.2014.
 */

@javax.persistence.Entity (name="Ttransaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;
    @ManyToOne
    @JoinColumn(name = "AccountID")
    private Account account;
    private int amount;
    private Date datetime;
    private double withdrawPercentage;

    public Transaction(Account account, int amount, Calendar datetime, float withdrawPercentage) {
        this.account = account;
        this.amount = amount;
        this.datetime = DBhelper.normalizeCal(datetime);
        this.withdrawPercentage = withdrawPercentage;
    }

    public Transaction() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public double getWithdrawPercentage() {
        return withdrawPercentage;
    }

    public void setWithdrawPercentage(double withdrawPercentage) {
        this.withdrawPercentage = withdrawPercentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (ID != that.ID) return false;
        if (amount != that.amount) return false;
        if (Double.compare(that.withdrawPercentage, withdrawPercentage) != 0) return false;
        if (!account.equals(that.account)) return false;
//        System.out.println(datetime.compareTo(new Date(that.datetime.getTime())) == 0);
//        System.out.println("before: " + this.datetime.getClass().getName() + this.datetime);
//        System.out.println("after: " + new Date(that.datetime.getTime()).getClass().getName() + new Date(that.datetime.getTime()));
        if (datetime.compareTo(that.datetime) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = ID;
        result = 31 * result + account.hashCode();
        result = 31 * result + amount;
        result = 31 * result + datetime.hashCode();
        temp = Double.doubleToLongBits(withdrawPercentage);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
