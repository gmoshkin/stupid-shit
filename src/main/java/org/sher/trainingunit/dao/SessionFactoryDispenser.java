package org.sher.trainingunit.dao;

import org.hibernate.cfg.Configuration;
import org.hibernate.SessionFactory;

public class SessionFactoryDispenser {
	private static SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
