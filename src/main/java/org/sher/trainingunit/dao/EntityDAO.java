package org.sher.trainingunit.dao;

import java.lang.Class;
import java.util.List;

import org.hibernate.Session;
import org.sher.trainingunit.dto.Entity;
import org.springframework.stereotype.Service;

import static org.sher.trainingunit.dao.DBhelper.queryWrapper;

@Service
public class EntityDAO {
	
	public <T extends Entity> int saveEntity(T e) {
//		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
//		Session session = null;
//		Integer id = null;
//		try {
//			session = sessionFactory.openSession();
//			session.beginTransaction();
//			id = (Integer) session.save(e);
//			session.getTransaction().commit();
//		} finally {
//			if (session != null && session.isOpen())
//				session.close();
//		}
        return (Integer) queryWrapper((Session session) -> {
            return session.save(e);
        });
	}
	
	public <T extends Entity> void updateEntity(T e) {
//		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
//		Session session = null;
//		try {
//			session = sessionFactory.openSession();
//			session.beginTransaction();
//			session.update(e);
//			session.getTransaction().commit();
//		} finally {
//			if (session != null && session.isOpen())
//				session.close();
//		}
        queryWrapper((Session session) -> {
            session.update(e);
            return null;
        });
	}
	
	public <T extends Entity> void deleteEntity(T entity) {
//		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
//		Session session = null;
//		try {
//			session = sessionFactory.openSession();
//			session.beginTransaction();
//			session.delete(entity);
//			session.getTransaction().commit();
//		} finally {
//			if (session != null && session.isOpen())
//				session.close();
//		}
        queryWrapper((Session session) -> {
            session.delete(entity);
            return null;
        });

    }
	
	@SuppressWarnings("unchecked")
	public <T extends Entity> T getEntity(Class<T> entityClass, int id) {
//		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
//		Session session = null;
//		T entity = null;
//		try {
//			session = sessionFactory.openSession();
//			session.beginTransaction();
//			entity = (T) session.get(entityClass, id);
//			session.getTransaction().commit();
//		} finally {
//			if (session != null && session.isOpen())
//				session.close();
//		}
		
		return (T) queryWrapper((Session session) -> {
            return session.get(entityClass, id);
        });
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Entity> List<T> getEntities(Class<T> entityClass) {
//		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
//		Session session = null;
//		List<T> list = null;
//		try {
//			session = sessionFactory.openSession();
//			session.beginTransaction();
//			list = session.createCriteria(entityClass).list();
//			session.getTransaction().commit();
//		} finally {
//			if (session != null && session.isOpen())
//				session.close();
//		}
		
		return (List<T>) queryWrapper((Session session) -> {
            return session.createCriteria(entityClass).list();
        });
	}
}
