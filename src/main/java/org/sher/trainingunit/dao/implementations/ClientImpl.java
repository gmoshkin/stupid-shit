package org.sher.trainingunit.dao.implementations;

import org.hibernate.Query;
import org.hibernate.Session;
import org.sher.trainingunit.dao.DBhelper;
import org.sher.trainingunit.dao.interfaces.ClientInterface;
import org.sher.trainingunit.dto.Account;
import org.sher.trainingunit.dto.Client;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arseny on 29.08.2014.
 */

@Service
public class ClientImpl implements ClientInterface {
    public void save(Client client) { DBhelper.queryWrapper((Session session) -> { session.save(client); return null;}); }
    public void update(Client client) { DBhelper.queryWrapper((Session session) -> { session.update(client); return null;}); }
    public void delete(Client client) { DBhelper.queryWrapper((Session session) -> {session.delete(client); return null;}); }
    public Client getById(int id) {
        Client res = (Client) DBhelper.queryWrapper((Session session) -> {
            return session.get(Client.class, id);
        });
        return res;
    }

    public List<Client> getAll() {
        @SuppressWarnings("unchecked")
        List<Client> res = (List<Client>) DBhelper.queryWrapper((Session session) -> {
            return session.createQuery("from Client").list();

        });
        return res;
    }

    public List<Client> searchByName(String name) {
        @SuppressWarnings("unchecked")
        List<Client> res = (List<Client>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Client as c where c.name like :name";
            Query query = session.createQuery(queryString)
                    .setParameter("name", "%" + name + "%");
            return query.list();
        });
        return res;
    }

    public List<Client> searchByDocument(int document) {
        @SuppressWarnings("unchecked")
        List<Client> res = (List<Client>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Client as c where c.document = :document";
            Query query = session.createQuery(queryString)
                    .setParameter("document", document);
            return query.list();
        });
        return res;
    }

    public List<Account> getAccounts(int clientID) {
        @SuppressWarnings("unchecked")
        List<Account> res = (List<Account>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Account as a where a.client.ID = :clientID";
            Query query = session.createQuery(queryString)
                    .setParameter("clientID", clientID);
            return query.list();
        });
        return res;
    }

    public List<Client> getLatest() {
        @SuppressWarnings("unchecked")
        List<Client> res = (List<Client>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Client as c order by c.id desc";
            Query query = session.createQuery(queryString)
                    .setMaxResults(10);
            return query.list();
        });
        return res;
    }
}
