package org.sher.trainingunit.dao.implementations;

import org.hibernate.Query;
import org.hibernate.Session;
import org.sher.trainingunit.dao.DBhelper;
import org.sher.trainingunit.dao.interfaces.AddressInterface;
import org.sher.trainingunit.dto.Address;
import org.sher.trainingunit.dto.Client;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arseny on 29.08.2014.
 */
@Service
public class AddressImpl implements AddressInterface {
    public void save(Address address) { DBhelper.queryWrapper((Session session) -> { session.save(address); return null;}); }
    public void update(Address address) { DBhelper.queryWrapper((Session session) -> { session.update(address); return null;}); }
    public void delete(Address address) { DBhelper.queryWrapper((Session session) -> {session.delete(address); return null;}); }
    public Address getById(String id) {
        Address res = (Address) DBhelper.queryWrapper((Session session) -> {
            return session.get(Address.class, id);
        });
        return res;
    }

    public List<Address> getAdressesById(int clientID) {
        @SuppressWarnings("unchecked")
        List<Address> res = (List<Address>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Address as a where a.client.ID = :clientID";
            Query query = session.createQuery(queryString)
                    .setParameter("clientID", clientID);
            return query.list();
        });
        return res;
    }

    public void updateAddress(Address address, String newAddress) {
        Client client = address.getClient();
        Address nA = new Address(newAddress, client);
        delete(address);
        save(nA);
    }
}
