package org.sher.trainingunit.dao.implementations;

import org.hibernate.Query;
import org.hibernate.Session;
import org.sher.trainingunit.dao.DBhelper;
import org.sher.trainingunit.dao.interfaces.TransactionInterface;
import org.sher.trainingunit.dto.Transaction;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arseny on 30.08.2014.
 */

@Service
public class TransactionImpl implements TransactionInterface {
    public void save(Transaction transaction) { DBhelper.queryWrapper((Session session) -> {
        session.save(transaction);
        return null;
    }); }
    public void update(Transaction transaction) { DBhelper.queryWrapper((Session session) -> {
        session.update(transaction);
        return null;
    }); }
    public void delete(Transaction transaction) { DBhelper.queryWrapper((Session session) -> {
        session.delete(transaction);
        return null;
    }); }
    public Transaction getById(int id) {
        Transaction res = (Transaction) DBhelper.queryWrapper((Session session) -> {
            return session.get(Transaction.class, id);
        });
        return res;
    }

    public List<Transaction> getTransactionsById(int accountID) {
        @SuppressWarnings("unchecked")
        List<Transaction> res = (List<Transaction>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Ttransaction as t where t.account.number = :accountID";
            Query query = session.createQuery(queryString)
                    .setParameter("accountID", accountID);
            return query.list();
        });
        return res;
    }

    public List<Transaction> getLatest() {
        @SuppressWarnings("unchecked")
        List<Transaction> res = (List<Transaction>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Ttransaction as t order by t.id desc";
            Query query = session.createQuery(queryString)
                    .setMaxResults(10);
            return query.list();
        });
        return res;
    }
}
