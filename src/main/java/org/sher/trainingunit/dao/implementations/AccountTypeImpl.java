package org.sher.trainingunit.dao.implementations;

import org.hibernate.Session;
import org.sher.trainingunit.dao.DBhelper;
import org.sher.trainingunit.dao.interfaces.AccountTypeInterface;
import org.sher.trainingunit.dto.AccountType;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arseny on 31.08.2014.
 */

@Service
public class AccountTypeImpl implements AccountTypeInterface {
    public void save(AccountType accountType) { DBhelper.queryWrapper((Session session) -> {
        session.save(accountType);
        return null;
    }); }
    public void update(AccountType accountType) { DBhelper.queryWrapper((Session session) -> {
        session.update(accountType);
        return null;
    }); }
    public void delete(AccountType accountType) { DBhelper.queryWrapper((Session session) -> {
        session.delete(accountType);
        return null;
    }); }
    public AccountType getById(int number) {
        AccountType res = (AccountType) DBhelper.queryWrapper((Session session) -> {
            return session.get(AccountType.class, number);
        });
        return res;
    }

    public List<AccountType> getAll() {
        @SuppressWarnings("unchecked")
        List<AccountType> res = (List<AccountType>) DBhelper.queryWrapper((Session session) -> {
            return session.createQuery("from AccountType").list();

        });
        return res;
    }
}
