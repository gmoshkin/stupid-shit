package org.sher.trainingunit.dao.implementations;

import org.hibernate.Query;
import org.hibernate.Session;
import org.sher.trainingunit.dao.DBhelper;
import org.sher.trainingunit.dao.interfaces.OfficeInterface;
import org.sher.trainingunit.dto.Client;
import org.sher.trainingunit.dto.Office;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arseny on 29.08.2014.
 */

@Service
public class OfficeImpl implements OfficeInterface {
    public void save(Office office) { DBhelper.queryWrapper((Session session) -> { session.save(office); return null;}); }
    public void update(Office office) { DBhelper.queryWrapper((Session session) -> { session.update(office); return null;}); }
    public void delete(Office office) { DBhelper.queryWrapper((Session session) -> {session.delete(office); return null;}); }
    public Office getById(int id) {
        Office res = (Office) DBhelper.queryWrapper((Session session) -> {
            return session.get(Office.class, id);
        });
        return res;
    }
    public List<Office> getAll() {
        @SuppressWarnings("unchecked")
        List<Office> res = (List<Office>) DBhelper.queryWrapper((Session session) -> {
                return session.createQuery("from Office").list();

        });
        return res;
    }

    public List<Office> searchByName(String name) {
        @SuppressWarnings("unchecked")
        List<Office> res = (List<Office>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Office as o where o.name like :name";
            Query query = session.createQuery(queryString)
                    .setParameter("name", "%" + name + "%");
            return query.list();
        });
        return res;
    }

    public List<Office> searchByAddress(String address) {
        @SuppressWarnings("unchecked")
        List<Office> res = (List<Office>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Office as o where o.address like :address";
            Query query = session.createQuery(queryString)
                    .setParameter("address", "%" + address + "%");
            return query.list();
        });
        return res;
    }

    public List<Client> getClients(int officeID) {
        @SuppressWarnings("unchecked")
        List<Client> res = (List<Client>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Client as c where c.office.ID = :officeID";
            Query query = session.createQuery(queryString)
                    .setParameter("officeID", officeID);
            return query.list();
        });
        return res;
    }
}
