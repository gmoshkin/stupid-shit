package org.sher.trainingunit.dao.implementations;

import org.hibernate.Query;
import org.hibernate.Session;
import org.sher.trainingunit.dao.DBhelper;
import org.sher.trainingunit.dao.interfaces.EmailInterface;
import org.sher.trainingunit.dto.Email;
import org.sher.trainingunit.dto.Client;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arseny on 30.08.2014.
 */
@Service
public class EmailImpl implements EmailInterface{
    public void save(Email email) { DBhelper.queryWrapper((Session session) -> {
        session.save(email);
        return null;
    }); }
    public void update(Email email) { DBhelper.queryWrapper((Session session) -> {
        session.update(email);
        return null;
    }); }
    public void delete(Email email) { DBhelper.queryWrapper((Session session) -> {
        session.delete(email);
        return null;
    }); }
    public Email getById(String id) {
        Email res = (Email) DBhelper.queryWrapper((Session session) -> {
            return session.get(Email.class, id);
        });
        return res;
    }

    public List<Email> getEmailsById(int clientID) {
        @SuppressWarnings("unchecked")
        List<Email> res = (List<Email>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Email as e where e.client.ID = :clientID";
            Query query = session.createQuery(queryString)
                    .setParameter("clientID", clientID);
            return query.list();
        });
        return res;
    }

    public void updateEmail(Email email, String newEmail) {
        Client client = email.getClient();
        Email nE = new Email(newEmail, client);
        delete(email);
        save(nE);
    }
}
