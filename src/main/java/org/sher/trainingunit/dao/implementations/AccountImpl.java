package org.sher.trainingunit.dao.implementations;

import org.hibernate.Query;
import org.hibernate.Session;
import org.sher.trainingunit.dao.DBhelper;
import org.sher.trainingunit.dao.interfaces.AccountInterface;
import org.sher.trainingunit.dto.Account;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arseny on 30.08.2014.
 */

@Service
public class AccountImpl implements AccountInterface{
    public void save(Account account) { DBhelper.queryWrapper((Session session) -> {
        session.save(account);
        return null;
    }); }
    public void update(Account account) { DBhelper.queryWrapper((Session session) -> {
        session.update(account);
        return null;
    }); }
    public void delete(Account account) { DBhelper.queryWrapper((Session session) -> {
        session.delete(account);
        return null;
    }); }
    public Account getById(int number) {
        Account res = (Account) DBhelper.queryWrapper((Session session) -> {
            return session.get(Account.class, number);
        });
        return res;
    }
    public List<Account> getAll() {
        @SuppressWarnings("unchecked")
        List<Account> res = (List<Account>) DBhelper.queryWrapper((Session session) -> {
            return session.createQuery("from Account").list();
        });
        return res;
    }

    public List<Account> getLatest() {
        @SuppressWarnings("unchecked")
        List<Account> res = (List<Account>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Account as a order by a.number desc";
            Query query = session.createQuery(queryString)
                    .setMaxResults(10);
            return query.list();
        });
        return res;
    }
}
