package org.sher.trainingunit.dao.implementations;

import org.hibernate.Query;
import org.hibernate.Session;
import org.sher.trainingunit.dao.DBhelper;
import org.sher.trainingunit.dao.interfaces.PhoneInterface;
import org.sher.trainingunit.dto.Client;
import org.sher.trainingunit.dto.Phone;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arseny on 30.08.2014.
 */
@Service
public class PhoneImpl implements PhoneInterface {
    public void save(Phone phone) { DBhelper.queryWrapper((Session session) -> {
        session.save(phone);
        return null;
    }); }
    public void update(Phone phone) { DBhelper.queryWrapper((Session session) -> {
        session.update(phone);
        return null;
    }); }
    public void delete(Phone phone) { DBhelper.queryWrapper((Session session) -> {
        session.delete(phone);
        return null;
    }); }
    public Phone getById(String id) {
        Phone res = (Phone) DBhelper.queryWrapper((Session session) -> {
            return session.get(Phone.class, id);
        });
        return res;
    }

    public List<Phone> getPhonesById(int clientID) {
        @SuppressWarnings("unchecked")
        List<Phone> res = (List<Phone>) DBhelper.queryWrapper((Session session) -> {
            String queryString = "from Phone as p where p.client.ID = :clientID";
            Query query = session.createQuery(queryString)
                    .setParameter("clientID", clientID);
            return query.list();
        });
        return res;
    }

    public void updatePhone(Phone phone, String newPhone) {
        Client client = phone.getClient();
        Phone nP = new Phone(newPhone, client);
        delete(phone);
        save(nP);
    }
}
