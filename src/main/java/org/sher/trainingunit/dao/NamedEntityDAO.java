package org.sher.trainingunit.dao;

import java.lang.Class;
import java.lang.String;
import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.sher.trainingunit.dto.NamedEntity;
import org.springframework.stereotype.Service;

@Service
public class NamedEntityDAO {
	@SuppressWarnings("unchecked")
	public <T extends NamedEntity> T getNamedEntity(Class<T> entityClass, String name) {
		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
		Session session = null;
		ArrayList<T> list = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			list = new ArrayList<T>(session.createCriteria(entityClass)
					.add(Restrictions.like("name", name)).list());
			session.getTransaction().commit();
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		
		if (list == null || list.isEmpty())
			return null;
		return list.get(0);
	}
}
