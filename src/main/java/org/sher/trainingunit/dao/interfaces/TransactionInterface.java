package org.sher.trainingunit.dao.interfaces;

import org.sher.trainingunit.dto.Transaction;

import java.util.List;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface TransactionInterface {
    void save(Transaction transaction);
    void update(Transaction transaction);
    void delete(Transaction transaction);
    Transaction getById(int id);
    List<Transaction> getTransactionsById(int accountID);
    List<Transaction> getLatest();
}
