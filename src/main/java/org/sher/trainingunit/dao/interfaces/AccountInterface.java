package org.sher.trainingunit.dao.interfaces;

import org.sher.trainingunit.dto.Account;

import java.util.List;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface AccountInterface {
    void save(Account account);
    void update(Account account);
    void delete(Account account);
    Account getById(int number);
    List<Account> getAll();
    List<Account> getLatest();
}
