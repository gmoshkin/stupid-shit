package org.sher.trainingunit.dao.interfaces;

import org.sher.trainingunit.dto.Account;
import org.sher.trainingunit.dto.Client;

import java.util.List;

/**
 * Created by Arseny on 29.08.2014.
 */
public interface ClientInterface {
    void save(Client client);
    void update(Client client);
    void delete(Client client);
    Client getById(int id);
    List<Client> getAll();
    List<Client> searchByName(String name);
    List<Client> searchByDocument(int document);
    List<Account> getAccounts(int clientID);
    List<Client> getLatest();
}
