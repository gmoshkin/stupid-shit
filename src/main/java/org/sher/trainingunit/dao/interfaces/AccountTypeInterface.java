package org.sher.trainingunit.dao.interfaces;

import org.sher.trainingunit.dto.AccountType;

import java.util.List;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface AccountTypeInterface {
    void save(AccountType accountType);
    void update(AccountType accountType);
    void delete(AccountType accountType);
    AccountType getById(int id);
    public List<AccountType> getAll();
}
