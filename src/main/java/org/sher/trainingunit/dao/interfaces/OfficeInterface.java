package org.sher.trainingunit.dao.interfaces;

import org.sher.trainingunit.dto.Client;
import org.sher.trainingunit.dto.Office;

import java.util.List;

/**
 * Created by Arseny on 29.08.2014.
 */
public interface OfficeInterface {
    void save(Office office);
    void update(Office office);
    void delete(Office office);
    Office getById(int id);
    List<Office> getAll();
    List<Office> searchByName(String name);
    List<Office> searchByAddress(String address);
    List<Client> getClients(int officeID);
}
