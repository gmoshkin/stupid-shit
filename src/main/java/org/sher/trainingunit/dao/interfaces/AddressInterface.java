package org.sher.trainingunit.dao.interfaces;

import org.sher.trainingunit.dto.Address;

import java.util.List;

/**
 * Created by Arseny on 29.08.2014.
 */
public interface AddressInterface {
    void save(Address address);
    void update(Address address);
    void delete(Address address);
    Address getById(String id);
    List<Address> getAdressesById(int clientID);
    public void updateAddress(Address address, String newAddress);
}
