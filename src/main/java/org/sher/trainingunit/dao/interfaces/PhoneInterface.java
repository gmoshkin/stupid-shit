package org.sher.trainingunit.dao.interfaces;

import org.sher.trainingunit.dto.Phone;

import java.util.List;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface PhoneInterface {
    void save(Phone phone);
    void update(Phone phone);
    void delete(Phone phone);
    Phone getById(String id);
    List<Phone> getPhonesById(int clientID);
    public void updatePhone(Phone phone, String newPhone);
}
