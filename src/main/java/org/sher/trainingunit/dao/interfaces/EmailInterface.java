package org.sher.trainingunit.dao.interfaces;

import org.sher.trainingunit.dto.Email;

import java.util.List;

/**
 * Created by Arseny on 30.08.2014.
 */
public interface EmailInterface {
    void save(Email email);
    void update(Email email);
    void delete(Email email);
    Email getById(String id);
    List<Email> getEmailsById(int clientID);
    public void updateEmail(Email address, String newEmail);
}
