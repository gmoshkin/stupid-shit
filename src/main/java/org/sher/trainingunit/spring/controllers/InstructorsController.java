package org.sher.trainingunit.spring.controllers;

import org.sher.trainingunit.dao.EntityDAO;
import org.sher.trainingunit.dto.Company;
import org.sher.trainingunit.dto.Course;
import org.sher.trainingunit.dto.Instructor;
import org.sher.trainingunit.dto.Instructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by ion on 10.09.2014.
 */

@Controller
@RequestMapping("/instructors")
public class InstructorsController {
	@Autowired
	private EntityDAO edao;

	@RequestMapping("")
	public String list(@RequestParam(required=false) Integer error, Model model) {
		List<Instructor> instructorsList = edao.getEntities(Instructor.class);
		model.addAttribute("instructorsList", instructorsList);
		model.addAttribute("title", "TrainingUnit | Instructors");
		return "instructors/list";
	}

	@RequestMapping("info")
	public String info(@RequestParam(required=true) int id, Model model) {
		Instructor instructor = edao.getEntity(Instructor.class, id);
		model.addAttribute("instructor", instructor);
		model.addAttribute("coursesList", instructor.getCourses());
		model.addAttribute("classesList", instructor.getClasses());
		model.addAttribute("title", "TrainingUnit | " + instructor.getFullName());
		return "instructors/info";
	}

	@RequestMapping("delete")
	public String delete(@RequestParam int id, Model model) {
		edao.deleteEntity(edao.getEntity(Instructor.class, id));
		return "redirect:/instructors";
	}

	@RequestMapping("add")
	public String add(@RequestParam(required = true) int companyId,
                      Model model) {
		model.addAttribute("companyId", companyId);
		model.addAttribute("title", "TrainingUnit | Add an instructor");
		return "instructors/add";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(@RequestParam(required=true) String firstName,
						   @RequestParam(required=true) String middleName,
						   @RequestParam(required=true) String lastName,
						   @RequestParam(required=true) String login,
						   @RequestParam(required=true) String password,
                           @RequestParam(required=true) int companyId,
						   Model model) {
        Instructor instructor = new Instructor(firstName, middleName, lastName, login, password);
        instructor.setCompany(edao.getEntity(Company.class, companyId));
		edao.saveEntity(instructor);
		return "redirect:/instructors";
	}

	@RequestMapping("edit")
	public String edit(@RequestParam int id, Model model) {
		model.addAttribute("instructor", edao.getEntity(Instructor.class, id));
		model.addAttribute("title", "TrainingUnit | Edit instructor");
		return "instructors/edit";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(@RequestParam(required=true) String firstName,
						 @RequestParam(required=true) String middleName,
						 @RequestParam(required=true) String lastName,
						 @RequestParam(required=true) String login,
                         @RequestParam(required=true) String password,
                         @RequestParam(required=true) int id,
                         Model model) {
		Instructor instructor = edao.getEntity(Instructor.class, id);
		instructor.setFirstName(firstName);
		instructor.setMiddleName(middleName);
		instructor.setLastName(lastName);
		instructor.setLogin(login);
		instructor.setPassword(password);
		edao.updateEntity(instructor);
		return "redirect:/instructors";
	}
    @RequestMapping("/removeCourse")
    public String removeCourse(@RequestParam(required = true) int instructorId,
                               @RequestParam(required = true) int courseId,
                               Model model) {
        Instructor instructor = edao.getEntity(Instructor.class, instructorId);
        Course course = edao.getEntity(Course.class, courseId);
        instructor.removeCourse(course);
        course.removeInstructor(instructor);
        edao.updateEntity(instructor);
        model.addAttribute("id", instructorId);
        return "redirect:/instructors/info";
    }
    @RequestMapping("/remove")
    public String remove(@RequestParam(required = true) int instructorId,
                         @RequestParam(required = true) int companyId,
                         Model model) {
        edao.deleteEntity(edao.getEntity(Instructor.class, instructorId));
        model.addAttribute("id", companyId);
        return "redirect:/companies/info";
    }
}
