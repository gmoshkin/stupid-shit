package org.sher.trainingunit.spring.controllers;

import org.sher.trainingunit.dao.EntityDAO;
import org.sher.trainingunit.dto.Company;
import org.sher.trainingunit.dto.Class;
import org.sher.trainingunit.dto.Course;
import org.sher.trainingunit.dto.Instructor;
import org.sher.trainingunit.dto.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ion on 10.09.2014.
 */

@Controller
@RequestMapping("/courses")
public class CoursesController {
    @Autowired
    private EntityDAO edao;

    @RequestMapping("/")
    public String list(@RequestParam(required=false) Integer error, Model model) {
        List<Course> coursesList = edao.getEntities(Course.class);
        model.addAttribute("coursesList", coursesList);
        model.addAttribute("title", "TrainingUnit | Courses");
        return "courses/list";
    }

    @RequestMapping("/info")
    public String info(@RequestParam(required=true) int id, Model model) {
        Course course = edao.getEntity(Course.class, id);
        String title = course.getName();

        // TODO fix the shit with the time
        model.addAttribute("course", course);
        model.addAttribute("classesList", course.getClasses());
        model.addAttribute("instructorsList", course.getInstructors());
        model.addAttribute("studentsList", course.getStudents());
        model.addAttribute("title", "TrainingUnit | " + title);
        return "courses/info";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam int id, Model model) {
        edao.deleteEntity(edao.getEntity(Course.class, id));
        return "redirect:/courses";
    }

    @RequestMapping("/add")
    public String add(@RequestParam int companyId, Model model) {
        model.addAttribute("companyId", companyId);
        model.addAttribute("title", "TrainingUnit | Add a course");
        return "courses/add";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@RequestParam(required=true) String name,
                           @RequestParam(required=true) String description,
                           @RequestParam(required=true) int companyId,
                           Model model) {
        Course course = new Course();
        course.setName(name);
        course.setDescription(description);
        course.setCompany(edao.getEntity(Company.class, companyId));
        edao.saveEntity(course);
        return "redirect:/courses";
    }

    @RequestMapping("/edit")
    public String edit(@RequestParam int id, Model model) {
        model.addAttribute("course", edao.getEntity(Course.class,id));
        model.addAttribute("title", "TrainingUnit | Edit course");
        return "courses/edit";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@RequestParam(required=true) String name,
                         @RequestParam(required=true) String description,
                         @RequestParam(required=true) int id,
                         Model model) {
        Course course = edao.getEntity(Course.class, id);
        course.setName(name);
        course.setDescription(description);
        edao.updateEntity(course);
        return "redirect:/courses";
    }
    @RequestMapping("/addInstructor")
    public String addInstructor(@RequestParam(required=true) int id, Model model) {
        Course course = edao.getEntity(Course.class, id);
        model.addAttribute("course", course);
        List<Instructor> instructorsList = new ArrayList<>(course.getCompany().getInstructors());
        instructorsList.removeAll(course.getInstructors());
        model.addAttribute("instructorsList", instructorsList);
        model.addAttribute("title", "TrainingUnit | Add an instructor");
        return "courses/addInstructor";
    }
    @RequestMapping("/addStudent")
    public String addStudent(@RequestParam(required=true) int id, Model model) {
        Course course = edao.getEntity(Course.class, id);
        model.addAttribute("course", course);
        List<Student> studentsList = edao.getEntities(Student.class);
        studentsList.removeAll(course.getStudents());
        model.addAttribute("studentsList", studentsList);
        model.addAttribute("title", "TrainingUnit | Add a student");
        return "courses/addStudent";
    }
    @RequestMapping("/registerInstructor")
    public String registerInstructor(@RequestParam(required=true) int instructorId,
                                    @RequestParam(required=true) int courseId,
                                    Model model) {
        Course course = edao.getEntity(Course.class, courseId);
        course.addInstructor(edao.getEntity(Instructor.class, instructorId));
        edao.updateEntity(course);
        model.addAttribute("id", course.getId());
        model.addAttribute("title", "TrainingUnit | " + course.getName());
        return "redirect:/courses/info";
    }
    @RequestMapping("/registerStudent")
    public String registerStudent(@RequestParam(required=true) int studentId,
                                     @RequestParam(required=true) int courseId,
                                     Model model) {
        Course course = edao.getEntity(Course.class, courseId);
        course.addStudent(edao.getEntity(Student.class, studentId));
        edao.updateEntity(course);
        model.addAttribute("id", course.getId());
        model.addAttribute("title", "TrainingUnit | " + course.getName());
        return "redirect:/courses/info";
    }
    @RequestMapping("/removeInstructor")
    public String removeInstructor(@RequestParam(required = true) int instructorId,
                                   @RequestParam(required = true) int courseId,
                                   Model model) {
        Instructor instructor = edao.getEntity(Instructor.class, instructorId);
        Course course = edao.getEntity(Course.class, courseId);
        course.removeInstructor(instructor);
        instructor.removeCourse(course);
        edao.updateEntity(instructor);
        model.addAttribute("id", courseId);
        return "redirect:/courses/info";
    }
    @RequestMapping("/removeStudent")
    public String removeStudent(@RequestParam(required = true) int studentId,
                                @RequestParam(required = true) int courseId,
                                Model model) {
        Student student = edao.getEntity(Student.class, studentId);
        Course course = edao.getEntity(Course.class, courseId);
        course.removeStudent(student);
        edao.updateEntity(course);
        model.addAttribute("id", courseId);
        return "redirect:/courses/info";
    }
}
