package org.sher.trainingunit.spring.controllers;

import jdk.nashorn.internal.objects.AccessorPropertyDescriptor;
import org.sher.trainingunit.dao.EntityDAO;
import org.sher.trainingunit.dto.Course;
import org.sher.trainingunit.dto.Instructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * Created by ion on 10.09.2014.
 */
@Controller
public class IndexController {
    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;
    private EntityDAO edao;

    //TODO do something with the home page, it looks like shit
//TODO add cancel button on add and edit pages
//TODO change table headers style
    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("title", "TrainingUnit | Home");
        return "misc/index";
    }
}
