package org.sher.trainingunit.spring.controllers;

import org.sher.trainingunit.dao.EntityDAO;
import org.sher.trainingunit.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import java.util.List;

/**
 * Created by ion on 10.09.2014.
 */

@Controller
@RequestMapping("/students")
public class StudentsController {
    @Autowired
    private EntityDAO edao;

    @RequestMapping("")
    public String list(@RequestParam(required=false) Integer error, Model model) {
        List<Student> studentsList = edao.getEntities(Student.class);
        model.addAttribute("studentsList", studentsList);
        model.addAttribute("title", "TrainingUnit | Students");
        return "students/list";
    }

    @RequestMapping("info")
    public String info(@RequestParam(required=true) int id, Model model) {
        Student student = edao.getEntity(Student.class, id);
        model.addAttribute("student", student);
        model.addAttribute("coursesList", student.getCourses());
        model.addAttribute("classesList", student.getClasses());
        model.addAttribute("title", "TrainingUnit | " + student.getFullName());
        return "students/info";
    }

    @RequestMapping("delete")
    public String delete(@RequestParam int id, Model model) {
        edao.deleteEntity(edao.getEntity(Student.class, id));
        return "redirect:/students";
    }

    @RequestMapping("add")
    public String add(Model model) {
        model.addAttribute("title", "TrainingUnit | Add a student");
        return "students/add";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@RequestParam(required=true) String firstName,
                           @RequestParam(required=true) String middleName,
                           @RequestParam(required=true) String lastName,
                           @RequestParam(required=true) String login,
                           @RequestParam(required=true) String password,
                           Model model) {
        edao.saveEntity(new Student(firstName, middleName, lastName, login, password));
        return "redirect:/students";
    }

    @RequestMapping("edit")
    public String edit(@RequestParam int id, Model model) {
        model.addAttribute("student", edao.getEntity(Student.class, id));
        model.addAttribute("title", "TrainingUnit | Edit student");
        return "students/edit";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@RequestParam(required=true) String firstName,
                         @RequestParam(required=true) String middleName,
                         @RequestParam(required=true) String lastName,
                         @RequestParam(required=true) String login,
                         @RequestParam(required=true) String password,
                         @RequestParam(required=true) int id,
                         Model model) {
        Student student = edao.getEntity(Student.class, id);
        student.setFirstName(firstName);
        student.setMiddleName(middleName);
        student.setLastName(lastName);
        student.setLogin(login);
        student.setPassword(password);
        edao.updateEntity(student);
        return "redirect:/students";
    }
}
