package org.sher.trainingunit.spring.controllers;

import org.sher.trainingunit.dao.EntityDAO;
import org.sher.trainingunit.dto.Company;
import org.sher.trainingunit.dto.Course;
import org.sher.trainingunit.dto.Instructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by ion on 10.09.2014.
 */

@Controller
@RequestMapping("/companies")
public class CompaniesController {
    @Autowired
    private EntityDAO edao;

    @RequestMapping("")
    public String list(@RequestParam(required=false) Integer error, Model model) {
        List<Company> companiesList = edao.getEntities(Company.class);
        model.addAttribute("companiesList", companiesList);
        model.addAttribute("title", "TrainingUnit | Companies");
        return "companies/list";
    }

    @RequestMapping("info")
    public String info(@RequestParam(required=true) int id, Model model) {
        Company company = edao.getEntity(Company.class, id);
        String title = company.getName();

        model.addAttribute("company", company);
        model.addAttribute("coursesList", company.getCourses());
        model.addAttribute("instructorsList", company.getInstructors());
        model.addAttribute("title", "TrainingUnit | " + title);
        return "companies/info";
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam int id, Model model) {
        edao.deleteEntity(edao.getEntity(Company.class, id));
        return "redirect:/companies";
    }

    @RequestMapping("/add")
    public String add(Model model) {
        model.addAttribute("title", "TrainingUnit | Add a company");
        return "companies/add";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@RequestParam(required=true) String name,
                           @RequestParam(required=true) String login,
                           @RequestParam(required=true) String password,
                           @RequestParam(required=true) String address,
                           Model model) {
        edao.saveEntity(new Company(name, login, password, address));
        return "redirect:/companies";
    }

    @RequestMapping("/edit")
    public String edit(@RequestParam int id, Model model) {
        model.addAttribute("company", edao.getEntity(Company.class, id));
        model.addAttribute("title", "TrainingUnit | Edit company");
        return "companies/edit";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@RequestParam(required=true) String name,
                         @RequestParam(required=true) String login,
                         @RequestParam(required=true) String password,
                         @RequestParam(required=true) String address,
                         @RequestParam(required=true) int id,
                         Model model) {
        Company company = edao.getEntity(Company.class, id);
        company.setName(name);
        company.setLogin(login);
        company.setPassword(password);
        company.setAddress(address);
        edao.updateEntity(company);
        return "redirect:/companies";
    }
//    @RequestMapping("/addInstructor")
//    public String add(@RequestParam(required=true) int id, Model model) {
//        model.addAttribute("company", edao.getEntity(Company.class, id));
//        model.addAttribute("instructorsList", edao.getEntities(Instructor.class));
//        model.addAttribute("title", "TrainingUnit | Add an instructor");
//        return "companies/addInstructor";
//    }
//@RequestMapping("/removeInstructor")
//public String removeInstructor(@RequestParam(required = true) int instructorId,
//                                     @RequestParam(required = true) int companyId,
//                                     Model model) {
//    Instructor instructor = edao.getEntity(Instructor.class, instructorId);
//    Company company = edao.getEntity(Company.class, companyId);
//    company.removeInstructor(instructor);
//    edao.updateEntity(company);
//    model.addAttribute("id", companyId);
//    return "redirect:/info";
//}
}
