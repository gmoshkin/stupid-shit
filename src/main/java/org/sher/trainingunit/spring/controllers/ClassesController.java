package org.sher.trainingunit.spring.controllers;

import org.sher.trainingunit.dao.EntityDAO;
import org.sher.trainingunit.dto.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by ion on 10.09.2014.
 */

@Controller
@RequestMapping("/courses")
public class ClassesController {
    @Autowired
    private EntityDAO edao;

    @RequestMapping("")
    public String list(@RequestParam(required=false) Integer error, Model model) {
        List<Course> coursesList = edao.getEntities(Course.class);
        model.addAttribute("coursesList", coursesList);
        model.addAttribute("title", "TrainingUnit | Courses");
        return "courses/list";
    }
}
