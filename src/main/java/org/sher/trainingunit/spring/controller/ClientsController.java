package org.sher.trainingunit.spring.controller;

import org.sher.trainingunit.dao.interfaces.*;
import org.sher.trainingunit.dao.interfaces.ClientInterface;
import org.sher.trainingunit.dto.Client;
import org.sher.trainingunit.dto.Office;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Arseny on 07.09.2014.
 */

@Controller
@RequestMapping("/clients")
public class ClientsController {

    @Autowired
    private ClientInterface ci;
    @Autowired
    private OfficeInterface oi;
    @Autowired
    private AddressInterface ai;
    @Autowired
    private EmailInterface ei;
    @Autowired
    private PhoneInterface pi;

    @RequestMapping("")
    public String clients(Model model) {
        List<Client> clientsList = ci.getAll();
        model.addAttribute("clientsList", clientsList);
        model.addAttribute("title", "Clients");
        return "clients/clients";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addClient(@RequestParam int officeID, Model model) {
//        Office office = oi.getById(officeID);
//        Client client = new Client();
//        client.setOffice(office);
//        client.setCreationDate(new Date());
//        model.addAttribute("command", client);
        model.addAttribute("officeID", officeID);
        model.addAttribute("title", "Adding client");
        return "clients/add";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerClient(@RequestParam(required=true) String name,
                                 @RequestParam(required=true) int document,
                                 @RequestParam(required=false) boolean isCorporated,
                                 @RequestParam(required=true) int officeID,
                                 Model model) {
      ci.save(new Client(document, name,  Calendar.getInstance(), oi.getById(officeID), isCorporated ));
        return "redirect:/clients";
    }

    @RequestMapping("edit")
    public String editClient(@RequestParam int id, Model model) {
        Client client = ci.getById(id);
        model.addAttribute("client", client);
        model.addAttribute("title", "Editing client");
        return "clients/edit";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String updateClient(@RequestParam(required=true) String name,
                                 @RequestParam(required=true) int document,
                                 @RequestParam(required=false) boolean isCorporated,
                                 @RequestParam(required=true) int clientID,
                                 Model model) {
        Client client = ci.getById(clientID);
        client.setDocument(document); client.setName(name);
        client.setType(isCorporated);
        ci.update(client);
        model.addAttribute("newOfficeSaved", 2);
        return "redirect:/clients";
    }

    @RequestMapping("delete")
    public String deleteClient(@RequestParam int id, Model model) {
        ci.delete(ci.getById(id));
        return "redirect:/clients";
    }

    @RequestMapping("search")
    public String clientsSearch(Model model) {
        model.addAttribute("title", "Searching clients");
        return "clients/search";
    }

    @RequestMapping(value = "searchResults", method = RequestMethod.POST)
    public String searchResults(
            @RequestParam(value="", required=false) String name,
            @RequestParam(value="", required=false) Integer document, Model model) {
        if (name == null && document == null) {
            model.addAttribute("officesList", new ArrayList<Office>());
        }
        else {

        }

        if (name == null) name = "";
        if (document == null && name.equals("")) {
            model.addAttribute("clientsList", new ArrayList<Client>());
        }
        else if (!name.equals("")) {
            model.addAttribute("clientsList", ci.searchByName(name));

        }
        else if (document != null) {
            model.addAttribute("clientsList", ci.searchByDocument(document));
        }
        model.addAttribute("title", "Searching results");
        return "clients/clients";
    }

    @RequestMapping(value = "/client", method = RequestMethod.GET)
    public String clientPage(@RequestParam int id, Model model) {
        model.addAttribute("client", ci.getById(id));
        model.addAttribute("accountsList", ci.getAccounts(id));
        model.addAttribute("addressesList", ai.getAdressesById(id));
        model.addAttribute("emailsList", ei.getEmailsById(id));
        model.addAttribute("phonesList", pi.getPhonesById(id));
        model.addAttribute("title", "Client " + ci.getById(id).getName());
        return "clients/client";
    }
}
