package org.sher.trainingunit.spring.controller;

import org.sher.trainingunit.dao.interfaces.OfficeInterface;
import org.sher.trainingunit.dto.Office;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arseny on 06.09.2014.
 */
@Controller
@RequestMapping("/offices")
public class OfficesController {

    @Autowired
    private OfficeInterface oi;

    @RequestMapping("")
    public String offices(Model model) {
        List<Office> officesList = oi.getAll();
        model.addAttribute("officesList", officesList);
        model.addAttribute("title", "Offices");
        return "offices/offices";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addOffice(Model model) {
        model.addAttribute("office", new Office());
        model.addAttribute("title", "Adding office");
        return "offices/add";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerOffice(@ModelAttribute Office office, Model model) {
        oi.save(office);
        model.addAttribute("actionWas", 1);
        return "redirect:/offices";
    }

    @RequestMapping("edit")
    public String editOffice(@RequestParam int id, Model model) {
        Office office = oi.getById(id);
        model.addAttribute("office", office);
        model.addAttribute("title", "Editing office");
        return "offices/edit";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String updateOffice(@ModelAttribute Office office, Model model) {
        oi.update(office);
        model.addAttribute("newOfficeSaved", 2);
        return "redirect:/offices";
    }

    @RequestMapping("delete")
    public String deleteOffice(@RequestParam int id, Model model) {
        oi.delete(oi.getById(id));
        return "redirect:/offices";
    }

    @RequestMapping("search")
    public String officesSearch(Model model) {
        model.addAttribute("title", "Searching offices");
        return "offices/search";
    }

    @RequestMapping(value = "searchResults", method = RequestMethod.POST)
    public String searchResults(
            @RequestParam(value="", required=false) String name,
            @RequestParam(value="", required=false) String address, Model model) {
        if (address == null) address = "";
        if (name == null) name = "";
        if (address.equals("") && name.equals("")) {
            model.addAttribute("officesList", new ArrayList<Office>());
        }
        else if (!name.equals("")) {
            model.addAttribute("officesList", oi.searchByName(name));

        }
        else if (!address.equals("")) {
            model.addAttribute("officesList", oi.searchByAddress(address));
        }
        model.addAttribute("title", "Search results");
        return "offices/offices";
    }

    @RequestMapping(value = "/office", method = RequestMethod.GET)
    public String officePage(@RequestParam int id, Model model) {
        model.addAttribute("office", oi.getById(id));
        model.addAttribute("clientsList", oi.getClients(id));
        model.addAttribute("title", "Office " + oi.getById(id).getName());
        return "offices/office";
    }
}
