package org.sher.trainingunit.spring.controller;

import org.sher.trainingunit.dao.interfaces.*;
import org.sher.trainingunit.dto.Address;
import org.sher.trainingunit.dto.Email;
import org.sher.trainingunit.dto.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Arseny on 08.09.2014.
 */

@Controller
public class AddrEmailPhoneController {

    @Autowired
    private ClientInterface ci;
    @Autowired
    private AddressInterface ai;
    @Autowired
    private EmailInterface ei;
    @Autowired
    private PhoneInterface pi;

    //addresses
    @RequestMapping("/addresses/add")
    public String addAddress(@RequestParam int clientID, Model model) {
        model.addAttribute("clientID", clientID);
        model.addAttribute("title", "Adding address");
        return "addresses/add";
    }

    @RequestMapping(value = "/addresses/register", method = RequestMethod.POST)
    public String registerAddress(@RequestParam(required=true) String address,
                                 @RequestParam(required=true) int clientID,
                                 Model model) {
        Address nA = new Address(address, ci.getById(clientID));
        ai.save(nA);
        String returnString =  "redirect:/clients/client?id=" + clientID;
        return "redirect:/clients";
    }

    @RequestMapping("/addresses/edit")
    public String editAddress(@RequestParam String address, Model model) {
        model.addAttribute("oldAddress", address);
        model.addAttribute("title", "Editing address");
        return "addresses/edit";
    }

    @RequestMapping(value = "/addresses/update", method = RequestMethod.POST)
    public String updateAddress(@RequestParam(required=true) String address,
                               @RequestParam(required=true) String oldAddress,
                               Model model) {
        ai.updateAddress(ai.getById(oldAddress), address);
        model.addAttribute("newOfficeSaved", 2);
        return "redirect:/clients";
    }

    @RequestMapping("/addresses/delete")
    public String deleteAddress(@RequestParam String address, Model model) {
        ai.delete(ai.getById(address));
        return "redirect:/clients";
    }

    //emails
    @RequestMapping("/emails/add")
    public String addEmail(@RequestParam int clientID, Model model) {
        model.addAttribute("clientID", clientID);
        model.addAttribute("title", "Adding email");
        return "emails/add";
    }

    @RequestMapping(value = "/emails/register", method = RequestMethod.POST)
    public String registerEmail(@RequestParam(required=true) String email,
                                  @RequestParam(required=true) int clientID,
                                  Model model) {
        Email nA = new Email(email, ci.getById(clientID));
        ei.save(nA);
        String returnString =  "redirect:/clients/client?id=" + clientID;
        return "redirect:/clients";
    }

    @RequestMapping("/emails/edit")
    public String editEmail(@RequestParam String email, Model model) {
        model.addAttribute("oldEmail", email);
        model.addAttribute("title", "Editing email");
        return "emails/edit";
    }

    @RequestMapping(value = "/emails/update", method = RequestMethod.POST)
    public String updateEmail(@RequestParam(required=true) String email,
                                @RequestParam(required=true) String oldEmail,
                                Model model) {
        ei.updateEmail(ei.getById(oldEmail), email);
        model.addAttribute("newOfficeSaved", 2);
        return "redirect:/clients";
    }

    @RequestMapping("/emailes/delete")
    public String deleteEmail(@RequestParam String email, Model model) {
        pi.delete(pi.getById(email));
        return "redirect:/clients";
    }

    //phones
    @RequestMapping("/phones/add")
    public String addPhone(@RequestParam int clientID, Model model) {
        model.addAttribute("clientID", clientID);
        model.addAttribute("title", "Adding phone");
        return "phones/add";
    }

    @RequestMapping(value = "/phones/register", method = RequestMethod.POST)
    public String registerPhone(@RequestParam(required=true) String phone,
                                @RequestParam(required=true) int clientID,
                                Model model) {
        Phone nP = new Phone(phone, ci.getById(clientID));
        pi.save(nP);
        String returnString =  "redirect:/clients/client?id=" + clientID;
        return "redirect:/clients";
    }

    @RequestMapping("/phones/edit")
    public String editPhone(@RequestParam String phone, Model model) {
        model.addAttribute("oldPhone", phone);
        model.addAttribute("title", "Editing phone");
        return "phones/edit";
    }

    @RequestMapping(value = "/phones/update", method = RequestMethod.POST)
    public String updatePhone(@RequestParam(required=true) String phone,
                              @RequestParam(required=true) String oldPhone,
                              Model model) {
        pi.updatePhone(pi.getById(oldPhone), phone);
        model.addAttribute("newOfficeSaved", 2);
        return "redirect:/clients";
    }

    @RequestMapping("/phonees/delete")
    public String deletePhone(@RequestParam String phone, Model model) {
        ei.delete(ei.getById(phone));
        return "redirect:/clients";
    }
}
