package org.sher.trainingunit.spring.controller;

import org.sher.trainingunit.dao.interfaces.AccountTypeInterface;
import org.sher.trainingunit.dto.AccountType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Arseny on 08.09.2014.
 */

@Controller
@RequestMapping("/accountTypes")
public class AccountTypesController {
    @Autowired
    private AccountTypeInterface ati;

    @RequestMapping("")
    public String accountTypes(Model model) {
        List<AccountType> AccountTypesList = ati.getAll();
        model.addAttribute("accountTypesList", AccountTypesList);
        model.addAttribute("title", "Account types");
        return "accountTypes/accountTypes";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addAccountType(Model model) {
        model.addAttribute("title", "Adding account type");
        return "accountTypes/add";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerClient(@RequestParam(required=true) int maxCredit,
                                 @RequestParam(required=true) int paybackDuration,
                                 @RequestParam(required=false) boolean isAvailableForNatural,
                                 @RequestParam(required=false) boolean isAvailableForCorporated,
                                 Model model) {
        AccountType at = new AccountType(maxCredit, paybackDuration, isAvailableForNatural, isAvailableForCorporated);
        ati.save(at);
        return "redirect:/accountTypes";
    }

    @RequestMapping("/delete")
    public String deleteAT(@RequestParam int id, Model model) {
        ati.delete(ati.getById(id));
        return "redirect:/accountTypes";
    }
}
