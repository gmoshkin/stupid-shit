package org.sher.trainingunit.spring.controller;

import org.sher.trainingunit.dao.interfaces.AccountInterface;
import org.sher.trainingunit.dao.interfaces.TransactionInterface;
import org.sher.trainingunit.dto.Account;
import org.sher.trainingunit.dto.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Arseny on 08.09.2014.
 */

@Controller
@RequestMapping("/transactions")
public class TransactionsController {
    @Autowired
    private TransactionInterface ti;
    @Autowired
    private AccountInterface ai;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addTransaction(@RequestParam int accountID, Model model) {
        model.addAttribute("accountID", accountID);
        model.addAttribute("title", "Making transaction");
        return "transactions/add";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerTransaction(@RequestParam(required=true) int amount,
                                  @RequestParam(required=true) Float withdrawPercentage,
                                  @RequestParam(required=true) int accountID,
                                  Model model) {
        if (withdrawPercentage == null) withdrawPercentage = (float) 0;
        Account a = ai.getById(accountID);
        int balanceBefore = a.getBalance();
        int amountAfterCommission = (int) (amount * (1 - withdrawPercentage));
        int balanceAfter = balanceBefore + amountAfterCommission;

        if (a.getPaybackDate() != null) {
            if ((new Date()).after(a.getPaybackDate())) {
                ai.delete(a);
                model.addAttribute("error", 1);
                return "redirect:/accounts";
            }
        }

        if (balanceBefore > 0 && balanceAfter < 0 ) {
            Calendar tmp = Calendar.getInstance();
            tmp.add(Calendar.DATE, a.getAccountType().getPaybackDuration());
            a.setPaybackDate(tmp.getTime());
        }

        if (balanceBefore < 0 && balanceAfter > 0) {
            a.setPaybackDate(null);
        }

        a.setBalance(balanceAfter);
        ai.update(a);
        Transaction transaction = new Transaction(a, amount, Calendar.getInstance(), withdrawPercentage);

        ti.save(transaction);
        model.addAttribute("id", a.getNumber());
        return "redirect:/accounts/account";
    }

    @RequestMapping("delete")
    public String deleteT(@RequestParam int id, Model model) {
        ti.delete(ti.getById(id));
        return "redirect:/accounts";
    }
}
