package org.sher.trainingunit.spring.controller;

import org.sher.trainingunit.dao.interfaces.AccountInterface;
import org.sher.trainingunit.dao.interfaces.AccountTypeInterface;
import org.sher.trainingunit.dao.interfaces.ClientInterface;
import org.sher.trainingunit.dao.interfaces.TransactionInterface;
import org.sher.trainingunit.dto.Account;
import org.sher.trainingunit.dto.AccountType;
import org.sher.trainingunit.dto.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Arseny on 08.09.2014.
 */

@Controller
@RequestMapping("/accounts")
public class AccountsController {
    @Autowired
    private ClientInterface ci;
    @Autowired
    private AccountInterface ai;
    @Autowired
    private AccountTypeInterface ati;
    @Autowired
    private TransactionInterface ti;

    @RequestMapping("")
    public String accounts(@RequestParam(required=false) Integer error, Model model) {
        String errorMessage = "";
        if (error != null) {
            if (error == 0)
                errorMessage = "Sorry, this account type is not available for this client";
            else if (error == 1)
                errorMessage = "The account has been destroyed due to the unpaid credit";
        }
        model.addAttribute("errorMessage", errorMessage);
        List<Account> accountsList = ai.getAll();
        model.addAttribute("accountsList", accountsList);
        model.addAttribute("title", "Accounts");
        return "accounts/accounts";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addAccount(@RequestParam int clientID, Model model) {
        model.addAttribute("clientID", clientID);
        model.addAttribute("title", "Creating account");
        return "accounts/add";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerAccount(@RequestParam(required=true) int balance,
                                 @RequestParam(required=true) int accountTypeID,
                                 @RequestParam(required=true) int clientID,
                                 Model model) {
        AccountType at = ati.getById(accountTypeID);
        Client client = ci.getById(clientID);
        if (client.getType() && !at.isAvailableForCorporated()) {
            model.addAttribute("error", 0);
            return "redirect:/accounts";
        }
        if (!client.getType() && !at.isAvailableForNatural()) {
            model.addAttribute("error", 0);
            return "redirect:/accounts";
        }
        ai.save(new Account(Calendar.getInstance(), ci.getById(clientID), balance, at));
        return "redirect:/accounts";
    }

    @RequestMapping("delete")
    public String deleteAccount(@RequestParam int id, Model model) {
        ai.delete(ai.getById(id));
        return "redirect:/accounts";
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public String accountPage(@RequestParam int id, Model model) {
        Account account = ai.getById(id);
        model.addAttribute("account", account);
        model.addAttribute("transactionsList", ti.getTransactionsById(id));
        model.addAttribute("title", "Account " + account.getNumber());
        return "accounts/account";
    }
}
