import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.sher.trainingunit.dao.implementations.*;
import org.sher.trainingunit.dao.interfaces.*;
import org.sher.trainingunit.dto.*;


/**
 * Created by Arseny on 23.08.2014.
 */

public class Test {
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    public static void main(String args []) {
//        char a_char = ' ';
//        int charCounter = 0;
//        int temp = 0;
//        String line = null;
//        String file = "C:\\matrix.txt";
//        BufferedReader reader = null;
//
//        ArrayList myIntArray = new ArrayList();
//
//        try {
//            reader = new BufferedReader(new FileReader(file));
//        } catch(FileNotFoundException fnfe) {
//            System.out.println(fnfe.getMessage());
//        }
//
//        try {
//            while ((line = reader.readLine()) != null) {
//                System.out.print(line + " |");
//                charCounter = 0;
//                int rawSize = 0;
//                while(charCounter < line.length()){
//                    a_char = line.charAt(charCounter);
//                    if(a_char != ' '){
//                        System.out.print(String.valueOf(a_char)+" ");
//                        temp = Character.digit(a_char, 10);
//                        System.out.print(temp);
//                        myIntArray.add(temp);
//                        System.out.println(String.valueOf(myIntArray.get(rawSize)));
//                        rawSize++;
//                    }
//                    charCounter++;
//                }
//                System.out.print("___________________");
//                for(int i = 0; i < myIntArray.size(); i++) {
//                    System.out.print(myIntArray.get(i));
//                }
//                System.out.print("___________________");
//            }
//        } catch(IOException ioe) {
//            System.out.println(ioe.getMessage());
//        }
        System.out.println("Hey");
    }
    private static SessionFactory createSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure();
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }
}
