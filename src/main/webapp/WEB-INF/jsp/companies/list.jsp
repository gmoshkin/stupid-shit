<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">
	${errorMessage}
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Address</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${companiesList}" var="company">
					<tr>
						<c:url value="/companies/info" var="companyURL">
							<c:param name="id" value="${company.getId()}" />
						</c:url>
						<td><a href="${companyURL}"><c:out value="${company.getName()}" /></td>
						<td><c:out value="${company.getAddress()}" /></td>
						<c:url value="/companies/edit" var="editURL">
							<c:param name="id" value="${company.getId()}" />
						</c:url>
						<td><a href="${editURL}">Edit</a></td>
						<c:url value="/companies/delete" var="deleteURL">
							<c:param name="id" value="${company.getId()}" />
						</c:url>
						<td><a href="${deleteURL}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="col-sm-8 col-md-9 sidebar">
            <c:url value="/companies/add" var="addCompanyURL"/>
        	<form action="${addCompanyURL}">
        	    <input class="btn btn-primary" type="submit" value="Add a company">
            </form>
        </div>
        <br>
	</div>
</div>