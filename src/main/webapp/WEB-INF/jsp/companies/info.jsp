<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">

		${company.getName()}

		<div>
			<table class="table table-bordered">
				<tr>
					<td>Administrator's Login</td>
					<td>${company.getLogin()}</td>
				</tr>
				<tr>
					<td>Administrator's Password</td>
					<td>${company.getPassword()}</td>
				</tr>
			 </table>
		</div>
		<table class="table sortable table-bordered">
			<thead>
				Courses
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${coursesList}" var="course">
					<tr>
						<c:url value="/courses/info" var="courseURL">
							<c:param name="id" value="${course.getId()}" />
						</c:url>
						<td><a href="${courseURL}"><c:out value="${course.getName()}"/></a></td>
						<td><c:out value="${course.getDescription()}" /></td>
						<c:url value="/courses/delete" var="deleteURL">
							<c:param name="courseId" value="${course.getId()}" />
							<c:param name="companyId" value="${company.getId()}" />
						</c:url>
						<td><a href="${deleteURL}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="col-sm-8 col-md-9 sidebar">
			<c:url value="/courses/add" var="addCourseURL"/>
			<form action="${addCourseURL}">
				<input type="hidden" name="companyId" value="${company.getId()}" />
				<input class="btn btn-primary" type="submit" value="Add a course">
			</form>
		</div>
		</br></br></br>

		Instructors

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${instructorsList}" var="instructor">
					<tr>
						<c:url value="/instructors/info" var="instructorURL">
							<c:param name="id" value="${instructor.getId()}" />
						</c:url>
						<td><a href="${instructorURL}"><c:out value="${instructor.getFullName()}" /></td>
						<c:url value="/instructors/remove" var="removeInstructorURL">
							<c:param name="instructorId" value="${instructor.getId()}" />
							<c:param name="companyId" value="${company.getId()}" />
						</c:url>
						<td><a href="${removeInstructorURL}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="col-sm-8 col-md-9 sidebar">
			<c:url value="/instructors/add" var="addInstructorURL"/>
			<form action="${addInstructorURL}">
				<input type="hidden" name="companyId" value="${company.getId()}" />
				<input class="btn btn-primary" type="submit" value="Add an instructor">
			</form>
		</div>
	</div>
</div>
