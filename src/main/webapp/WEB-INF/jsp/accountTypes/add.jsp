<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="row">
	<div class="col-sm-4 col-md-3 sidebar">
		<div class="mini-submenu">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</div>
		<div class="list-group">
			<span href="#" class="list-group-item active"> What do you
				want? <span class="pull-right" id="slide-submenu"> <i
					class="fa fa-times"></i>
			</span>
			</span> <a href="<c:url value="/accountTypes"/>" class="list-group-item">
				<i class="fa fa-arrow-left"></i> To account types list
			</a> <a href="<c:url value="/accountTypes/add"/>" class="list-group-item">
				<i class="fa fa-plus"></i> Add an account type
			</a>
		</div>
	</div>
	<div class="col-sm-12 col-md-12 sidebar">
		<c:url var="post_url" value="/accountTypes/register" />
	<form role="form" method="POST" action="${post_url}">
		<div class="form-group">
			<label for="maxCredit">Maximum credit</label> <input
				class="form-control" type="text" id="maxCredit" name="maxCredit" />
		</div>
		<div class="form-group">
			<label for="paybackDuration">Payback duration</label> <input
				class="form-control" type="text" id="paybackDuration"
				name="paybackDuration" />
		</div>
		<div class="checkbox">
			<label> <input type="checkbox" name="isAvailableForNatural">
				Available for natural
			</label>
		</div>
		<div class="checkbox">
			<label> <input type="checkbox" name="isAvailableForCorporated">
				Available for corporate
			</label>
		</div>
		<input class="btn btn-primary" type="submit" value="Add">
	</form>
	</div>
</div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>