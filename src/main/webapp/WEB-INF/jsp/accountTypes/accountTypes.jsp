<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="row">
	<div class="col-sm-4 col-md-3 sidebar">
		<div class="mini-submenu">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</div>
		<div class="list-group">
			<span href="#" class="list-group-item active"> What do you
				want? <span class="pull-right" id="slide-submenu"> <i
					class="fa fa-times"></i>
			</span>
			</span> <a href="<c:url value="/accountTypes/add"/>" class="list-group-item">
				<i class="fa fa-plus"></i> Add an account type
			</a>
		</div>
	</div>
	<div class="col-sm-8 col-md-9 sidebar">
	This is the list of all available account types. If a client runs into negative balance, he must refund it within payback duration period.
	</br></br></br>
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Number</th>
					<th>Max credit</th>
					<th>Payback duration (in days)</th>
					<th>Available for natural?</th>
					<th>Available for corporated?</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${accountTypesList}" var="at">
					<tr>
						<td><c:out value="${at.getID()}" /></td>
						<td><c:out value="${at.getMaxCredit()}" /></td>
						<td><c:out value="${at.getPaybackDuration()}" /></td>
						<td><c:choose>
								<c:when test="${at.isAvailableForNatural() == true}">Yes</c:when>
								<c:otherwise>No</c:otherwise>
							</c:choose></td>
						<td><c:choose>
								<c:when test="${at.isAvailableForCorporated() == true}">Yes</c:when>
								<c:otherwise>No</c:otherwise>
							</c:choose></td>

						<c:url value="/accountTypes/delete" var="deleteURL">
							<c:param name="id" value="${at.getID()}" />
						</c:url>
						<td><a href="${deleteURL}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
	</div>
</div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>