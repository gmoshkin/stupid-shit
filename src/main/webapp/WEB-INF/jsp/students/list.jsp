<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">
	${errorMessage}
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<div style="background:#FFCC99">
    			<tbody>
	    			<c:forEach items="${studentsList}" var="student">
		    			<tr>
	    					<c:url value="/students/info" var="infoURL">
		    					<c:param name="id" value="${student.getId()}" />
			    			</c:url>
			    			<td><a href="${infoURL}">${student.getFullName()}</td>
						    <c:url value="/students/delete" var="deleteURL">
							    <c:param name="id" value="${student.getId()}" />
    						</c:url>
				    		<td><a href="${deleteURL}">Delete</a></td>
	    					<c:url value="/students/edit" var="editURL">
		    					<c:param name="id" value="${student.getId()}" />
			    			</c:url>
				    		<td><a href="${editURL}">Edit</a></td>
    					</tr>
	    			</c:forEach>
		    	</tbody>
			</div>
		</table>
		<div class="col-sm-8 col-md-9 sidebar">
        	<c:url value="/students/add" var="addStudentURL"/>
	    	<form action="${addStudentURL}">
				<input class="btn btn-primary" type="submit" value="Add a student">
            </form>
        </div>
        <br>
	</div>
</div>
