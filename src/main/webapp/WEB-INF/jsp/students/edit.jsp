<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">
		<c:url var="post_url" value="/students/update" />
		<form role="form"  method="POST" action="${post_url}">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<input class="form-control" value="${student.getFirstName()}" placeholder="${student.getFirstName()}"
				    type="text" id="firstName" name="firstName" >
			</div>
			<div class="form-group">
				<label for="middleName">Middle Name</label>
				<input class="form-control" value="${student.getMiddleName()}" placeholder="${student.getMiddleName()}"
				    type="text" id="middleName" name="middleName" >
			</div>
			<div class="form-group">
				<label for="lastName">Last Name</label>
				<input class="form-control" value="${student.getLastName()}" placeholder="${student.getLastName()}"
				    type="text" id="lastName" name="lastName" >
			</div>
			<div class="form-group">
				<label for="login">Login</label>
				<input class="form-control" value="${student.getLogin()}" placeholder="${student.getLogin()}"
				    type="text" id="login" name="login" >
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input class="form-control" value="${student.getPassword()}" placeholder="${student.getPassword()}"
				    type="text" id="password" name="password" >
			</div>
			<input type="hidden" name="id" value="${student.getId()}" />
			<input class="btn btn-primary" type="submit" value="Update">
		</form>
	</div>
</div>