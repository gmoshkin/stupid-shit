<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">

        ${student.getFullName()}

        <div>
            <table class="table table-bordered">
                <tr>
                    <td>Login</td>
                    <td>${student.getLogin()}</td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>${student.getPassword()}</td>
                </tr>
             </table>
        </div>
		<table class="table sortable table-bordered">
			<thead>
			    Courses
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Company</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${coursesList}" var="course">
					<tr>
                   		<c:url value="/courses/info" var="courseURL">
                            <c:param name="id" value="${course.getId()}" />
                        </c:url>
                        <td><a href="${courseURL}"><c:out value="${course.getName()}"/></a></td>
						<td><c:out value="${course.getDescription()}" /></td>
						<c:url value="/companies/info" var="companyURL">
                        	<c:param name="id" value="${course.getCompany().getId()}" />
                        </c:url>
                        <td><a href="${companyURL}">${course.getCompany().getName()} </a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</br></br></br>

		Classes

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Course</th>
					<th>Date</th>
					<%--<th>Time</th>--%>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${classesList}" var="klass">
					<tr>
						<td><c:out value="${klass.getCourse().getName()}" /></td>
						<td><c:out value="${klass.getDate()}" /></td>
						<%--<td><c:out value="${klass.getTimeInterval()}" /></td>--%>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
