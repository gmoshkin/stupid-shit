<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="row">
	<div class="col-sm-4 col-md-3 sidebar">
		<div class="mini-submenu">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</div>
		<div class="list-group">
			<span href="#" class="list-group-item active"> What do you
				want? <span class="pull-right" id="slide-submenu"> <i
					class="fa fa-times"></i>
			</span>
			</span> <a href="<c:url value="/accounts"/>" class="list-group-item"> <i
				class="fa fa-arrow-left"></i> To accounts list
			</a>
		</div>
	</div>
	<div class="col-sm-8 col-md-9 sidebar">
		<c:url var="post_url" value="/accounts/register" />
		<form role="form"  method="POST"
			action="${post_url}">
			<div class="form-group">
				<label for="balance">Balance</label> <input class="form-control" placeholder="100" type="text" id="balance" name="balance" />
			</div>
			<div class="form-group">
				<label for="accountTypeID">Account Type ID. You can always see available types and their ID's <a href="<c:url value="/accountTypes"/>">here</a></label>
				 <input class="form-control" placeholder="1" type="text" id="accountTypeID" name="accountTypeID" />
			</div>
			<input type="hidden" name="clientID" value="${clientID}" /> 
			<input class="btn btn-primary" type="submit" value="Add">
		</form>
	</div>
</div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>