<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">
	${errorMessage}
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Company</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${coursesList}" var="course">
					<tr>
						<c:url value="/courses/info" var="courseURL">
							<c:param name="id" value="${course.getId()}" />
						</c:url>
						<td><a href="${courseURL}"><c:out value="${course.getName()}" /></td>
						<td><c:out value="${course.getDescription()}" /></td>
						<c:url value="/companies/company" var="companyURL">
                        	<c:param name="id" value="${course.getCompany().getId()}" />
                        </c:url>
                        <td><a href="${companyURL}">${course.getCompany().getName()} </a></td>
						<c:url value="/courses/edit" var="editURL">
							<c:param name="id" value="${course.getId()}" />
						</c:url>
						<td><a href="${editURL}">Edit</a></td>
						<c:url value="/courses/delete" var="deleteURL">
							<c:param name="id" value="${course.getId()}" />
						</c:url>
						<td><a href="${deleteURL}">Delete</a></td>
						<c:url value="/courses/info" var="courseURL">
							<c:param name="id" value="${course.getId()}" />
						</c:url>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>