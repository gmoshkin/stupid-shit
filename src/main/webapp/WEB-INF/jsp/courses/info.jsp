<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">

		${course.getName()}

		<div>
			<table class="table table-bordered">
				<tr>
					<td>Description</td>
					<td>${course.getDescription()}</td>
				</tr>
				<tr>
					<td>Company</td>
					<c:url value="/companies/info" var="companyURL">
						<c:param name="id" value="${course.getCompany().getId()}" />
					</c:url>
                    <td><a href="${companyURL}">${course.getCompany().getName()}</a></td>
				</tr>
			 </table>
		</div>

		Instructors

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${instructorsList}" var="instructor">
					<tr>
						<c:url value="/instructors/info" var="instructorURL">
							<c:param name="id" value="${instructor.getId()}" />
						</c:url>
						<td><a href="${instructorURL}"><c:out value="${instructor.getFullName()}" /></td>
						<c:url value="/courses/removeInstructor" var="removeInstructorURL">
							<c:param name="instructorId" value="${instructor.getId()}" />
							<c:param name="courseId" value="${course.getId()}" />
						</c:url>
						<td><a href="${removeInstructorURL}">Remove</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="col-sm-8 col-md-9 sidebar">
			<c:url value="/courses/addInstructor" var="addInstructorURL"/>
			<form action="${addInstructorURL}">
				<input type="hidden" name="id" value="${course.getId()}" />
				<input class="btn btn-primary" type="submit" value="Add an instructor">
			</form>
		</div>
		</br></br></br>

    	Students

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${studentsList}" var="student">
					<tr>
						<c:url value="/student/info" var="studentURL">
							<c:param name="id" value="${student.getId()}" />
						</c:url>
						<td><a href="${studentURL}"><c:out value="${student.getFullName()}" /></td>
						<c:url value="/courses/removeStudent" var="courseRemoveStudentURL">
							<c:param name="studentId" value="${student.getId()}" />
							<c:param name="courseId" value="${course.getId()}" />
						</c:url>
						<td><a href="${courseRemoveStudentURL}"><c:out value="Remove"/></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="col-sm-8 col-md-9 sidebar">
			<c:url value="/courses/addStudent" var="addStudentURL"/>
			<form action="${addStudentURL}">
				<input type="hidden" name="id" value="${course.getId()}" />
				<input class="btn btn-primary" type="submit" value="Add a student">
			</form>
		</div>
		</br></br></br>

		Classes

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Date</th>
					<th></th>
					<%--<th>Time</th>--%>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${classesList}" var="klass">
					<tr>
						<td><c:out value="${klass.getDate()}" /></td>
						<c:url value="/classes/delete" var="deleteClassURL">
							<c:param name="classId" value="${klass.getId()}" />
						</c:url>
						<td><a href="${deleteClassURL}">Delete</a></td>
						<%--<td><c:out value="${klass.getTimeInterval()}" /></td>--%>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="col-sm-8 col-md-9 sidebar">
			<c:url value="/classes/add" var="addClassURL"/>
			<form action="${addClassURL}">
				<input type="hidden" name="id" value="${course.getId()}" />
				<input class="btn btn-primary" type="submit" value="Add a class">
			</form>
		</div>
		</br></br></br>
	</div>
</div>
