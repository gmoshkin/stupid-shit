<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">
		<c:url var="post_url" value="/courses/registerInstructor" />
		<form role="form"  method="POST" action="${post_url}">
			<c:forEach items="${instructorsList}" var="instructor">
    			<div class="form-group">
	    			<input class="control" placeholder="" type="radio" name="instructorId" value="${instructor.getId()}">
					<c:out value="${instructor.getFullName()}"/>
			    </div>
			</c:forEach>
		    <input type="hidden" name="courseId" value="${course.getId()}" />
   			<input class="btn btn-primary" type="submit" value="Add">
		</form>
	</div>
</div>