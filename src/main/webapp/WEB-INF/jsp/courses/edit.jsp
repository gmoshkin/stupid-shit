<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">
		<c:url var="post_url" value="/courses/update" />
		<form role="form"  method="POST" action="${post_url}">
			<div class="form-group">
				<label for="name">Name</label>
				<input class="form-control" value="${course.getName()}" placeholder="${course.getName()}"
				    type="text" id="name" name="name" >
			</div>
			<div class="form-group">
				<label for="description"Description</label>
				<input class="form-control" value="${course.getDescription()}" placeholder="${course.getDescription()}"
				    type="text" id="description" name="description" >
			</div>
			<input type="hidden" name="id" value="${course.getId()}" />
			<input class="btn btn-primary" type="submit" value="Update">
		</form>
	</div>
</div>