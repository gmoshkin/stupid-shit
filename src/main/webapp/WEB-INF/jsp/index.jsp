<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="row">
	<div class="col-sm-4 col-md-3 sidebar">
		<div class="mini-submenu">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</div>
		<div class="list-group">
			<span href="#" class="list-group-item active"> What do you
				want? <span class="pull-right" id="slide-submenu"> <i
					class="fa fa-times"></i>
			</span>
			</span> <a href="<c:url value="/endPoints"/>" class="list-group-item"> <i
				class="fa fa-binoculars"></i> Controllers mapping
			</a>
		</div>
	</div>
	<div >class="col-sm-8 col-md-9 sidebar"
		Most recent transactions:

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Number</th>
					<th>Account</th>
					<th>Datetime of operation</th>
					<th>Amount</th>
					<th>Withdraw percentage</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${transactionsList}" var="transaction">
					<tr>
						<td><c:out value="${transaction.getID()}" /></td>
						<c:url value="/accounts/account" var="accountURL">
							<c:param name="id"
								value="${transaction.getAccount().getNumber()}" />
						</c:url>
						<td><a href="${accountURL}">Account</a></td>
						<td><c:out value="${transaction.getDatetime()}" /></td>
						<td><c:out value="${transaction.getAmount()}" /></td>
						<td><c:out value="${transaction.getWithdrawPercentage()}" /></td>

					</tr>
				</c:forEach>
			</tbody>
		</table>

		Newest accounts:
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Number</th>
					<th>Creation date</th>
					<th>Client</th>
					<th>Balance</th>
					<th>Payback date</th>
					<th>Account type ID</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${accountsList}" var="account">
					<tr>
						<td><c:out value="${account.getNumber()}" /></td>
						<td><c:out value="${account.getCreationDate()}" /></td>
						<c:url value="/clients/client" var="clientURL">
							<c:param name="id" value="${account.getClient().getID()}" />
						</c:url>
						<td><a href="${clientURL}">
								${account.getClient().getName()} </a></td>
						<td><c:out value="${account.getBalance()}" /></td>
						<td><c:out value="${account.getPaybackDate()}" /></td>
						<td><c:out value="${account.getAccountType().getID()}" /></td>
						<c:url value="/accounts/delete" var="deleteURL">
							<c:param name="id" value="${account.getNumber()}" />
						</c:url>
						<c:url value="/accounts/account" var="accountURL">
							<c:param name="id" value="${account.getNumber()}" />
						</c:url>
						<td><a href="${deleteURL}">Delete</a></td>
						<td><a href="${accountURL}">Account's page</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

			Newest clients:

			<table class="table sortable table-bordered">
				<thead>
					<tr>
						<th>Name</th>
						<th>Document</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${clientsList}" var="client">
						<tr>
							<td><c:out value="${client.getName()}" /></td>
							<td><c:out value="${client.getDocument()}" /></td>
							<c:url value="/clients/edit" var="editURL">
								<c:param name="id" value="${client.getID()}" />
							</c:url>
							<c:url value="/clients/delete" var="deleteURL">
								<c:param name="id" value="${client.getID()}" />
							</c:url>
							<c:url value="/clients/client" var="officeURL">
								<c:param name="id" value="${client.getID()}" />
							</c:url>
							<td><a href="${editURL}">Edit</a></td>
							<td><a href="${deleteURL}">Delete</a></td>
							<td><a href="${officeURL}">Client's page</a></td>
						</tr>
					</c:forEach>
				</tbody>
		</table>
	</div>
</div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>