<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="row">
	<div class="col-sm-4 col-md-3 sidebar">
		<div class="mini-submenu">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</div>
		<div class="list-group">
			<span href="#" class="list-group-item active"> What do you
				want? <span class="pull-right" id="slide-submenu"> <i
					class="fa fa-times"></i>
			</span>
			</span> <a href="<c:url value="/accounts"/>" class="list-group-item"> <i
				class="fa fa-arrow-left"></i> To accounts list
			</a>
		</div>
	</div>
	<div class="col-sm-8 col-md-9 sidebar">
		<c:url var="post_url" value="/transactions/register" />
		<form role="form"  method="POST"
			action="${post_url}">
			<div class="form-group">
				<label for="amount">Amount</label> <input class="form-control" placeholder="100" type="text" id="amount" name="amount" />
			</div>
			<div class="form-group">
				<label for="withdrawPercentage"> Commission percentage</label>
				 <input class="form-control" placeholder="0.1" type="text" id="withdrawPercentage" name="withdrawPercentage" />
			</div>
			<input type="hidden" name="accountID" value="${accountID}" /> 
			<input class="btn btn-primary" type="submit" value="Make transaction">
		</form>
	</div>
</div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>