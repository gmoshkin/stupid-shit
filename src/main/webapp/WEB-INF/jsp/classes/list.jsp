<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">
	${errorMessage}
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Middle Name</th>
					<th>Last Name</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${classesList}" var="class">
					<tr>
						<td><c:out value="${class.getFirstName()}" /></td>
						<td><c:out value="${class.getMiddleName()}" /></td>
						<td><c:out value="${class.getLastName()}" /></td>
						<c:url value="/classes/delete" var="deleteURL">
							<c:param name="id" value="${class.getId()}" />
						</c:url>
						<c:url value="/classes/info" var="classURL">
							<c:param name="id" value="${class.getId()}" />
						</c:url>
						<td><a href="${deleteURL}">Delete</a></td>
						<td><a href="${classURL}">Info</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>