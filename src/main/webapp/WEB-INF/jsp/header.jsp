<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
<title>${title}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">
<script src="js/jquery-2.1.0.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/sorttable.js"></script>
<link href="<c:url value="/css/style.css"/>" rel="stylesheet">
<link
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
</head>
<body>

<div class="container">
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<c:url value="/"/>">Home</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<% String jspPath = request.getRequestURI().toLowerCase(); %>
				<ul class="nav navbar-nav">
					<!--  Some porn -->
					<li <% if (jspPath.contains("client")) { %> class="active" <% } %>><a
						href="<c:url value="/clients"/>">Clients</a></li>
					<li <% if (jspPath.contains("office")) { %> class="active" <% } %>><a
						href="<c:url value="/offices"/>">Departments</a></li>
					<li <% if (jspPath.contains("account") && (!jspPath.contains("type"))) { %> class="active" <% } %>><a
						href="<c:url value="/accounts"/>">Accounts</a></li>
					<li <% if (jspPath.contains("type")) { %> class="active" <% } %>><a
						href="<c:url value="/accountTypes"/>">Account types</a></li>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>