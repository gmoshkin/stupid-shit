<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">
		<c:url var="post_url" value="/instructors/update" />
		<form role="form"  method="POST" action="${post_url}">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<input class="form-control" value="${instructor.getFirstName()}" placeholder="${instructor.getFirstName()}"
				    type="text" id="firstName" name="firstName" >
			</div>
			<div class="form-group">
				<label for="middleName">Middle Name</label>
				<input class="form-control" value="${instructor.getMiddleName()}" placeholder="${instructor.getMiddleName()}"
				    type="text" id="middleName" name="middleName" >
			</div>
			<div class="form-group">
				<label for="lastName">Last Name</label>
				<input class="form-control" value="${instructor.getLastName()}" placeholder="${instructor.getLastName()}"
				    type="text" id="lastName" name="lastName" >
			</div>
			<div class="form-group">
				<label for="login">Login</label>
				<input class="form-control" value="${instructor.getLogin()}" placeholder="${instructor.getLogin()}"
				    type="text" id="login" name="login" >
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input class="form-control" value="${instructor.getPassword()}" placeholder="${instructor.getPassword()}"
				    type="text" id="password" name="password" >
			</div>
			<input type="hidden" name="id" value="${instructor.getId()}" />
			<input class="btn btn-primary" type="submit" value="Update">
		</form>
	</div>
</div>