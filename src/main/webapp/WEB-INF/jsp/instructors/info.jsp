<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">

        ${instructor.getFullName()}

        <div>
            <table class="table table-bordered">
                <tr>
                    <td>Login</td>
                    <td>${instructor.getLogin()}</td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>${instructor.getPassword()}</td>
                </tr>
             </table>
        </div>
		<table class="table sortable table-bordered">
			<thead>
			    Courses
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Company</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${coursesList}" var="course">
					<tr>
                   		<c:url value="/courses/info" var="courseURL">
                            <c:param name="id" value="${course.getId()}" />
                        </c:url>
                        <td><a href="${courseURL}"><c:out value="${course.getName()}"/></a></td>
						<td><c:out value="${course.getDescription()}" /></td>
						<c:url value="/companies/info" var="companyURL">
                        	<c:param name="id" value="${course.getCompany().getId()}" />
                        </c:url>
                        <td><a href="${companyURL}">${course.getCompany().getName()}</a></td>
						<c:url value="/instructors/removeCourse" var="removeCourseURL">
							<c:param name="instructorId" value="${instructor.getId()}" />
							<c:param name="courseId" value="${course.getId()}" />
						</c:url>
						<td><a href="${removeCourseURL}">Remove</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="col-sm-8 col-md-9 sidebar">
			<c:url value="/instructors/addCourse" var="addCourseURL"/>
			<form action="${addCourseURL}">
				<input type="hidden" name="companyId" value="${company.getId()}" />
				<input class="btn btn-primary" type="submit" value="Add a course">
			</form>
		</div>
		</br></br></br>

		Classes

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Course</th>
					<th>Date</th>
					<%--<th>Time</th>--%>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${classesList}" var="klass">
					<tr>
						<td><c:out value="${klass.getCourse().getName()}" /></td>
						<td><c:out value="${klass.getDate()}" /></td>
						<%--<td><c:out value="${klass.getTimeInterval()}" /></td>--%>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
