<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/misc/include.jsp"%>
<%@ include file="/WEB-INF/jsp/misc/header.jsp"%>

<div class="row">
	<div class="col-sm-8 col-md-9 sidebar">
	${errorMessage}
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Company</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${instructorsList}" var="instructor">
					<tr>
						<c:url value="/instructors/info" var="instructorURL">
							<c:param name="id" value="${instructor.getId()}" />
						</c:url>
						<td><a href="${instructorURL}"><c:out value="${instructor.getFullName()}" /></td>
						<c:url value="/companies/info" var="companyURL">
                        	<c:param name="id" value="${instructor.getCompany().getId()}" />
                        </c:url>
						<td><a href="${companyURL}"><c:out value="${instructor.getCompany().getName()}" /></a></td>
						<c:url value="/instructors/edit" var="editURL">
							<c:param name="id" value="${instructor.getId()}" />
						</c:url>
						<td><a href="${editURL}">Edit</a></td>
						<c:url value="/instructors/delete" var="deleteURL">
							<c:param name="id" value="${instructor.getId()}" />
						</c:url>
						<td><a href="${deleteURL}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
