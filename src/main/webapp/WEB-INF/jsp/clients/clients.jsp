<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="row">
	<div class="col-sm-4 col-md-3 sidebar">
		<div class="mini-submenu">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</div>
		<div class="list-group">
			<span href="#" class="list-group-item active"> What do you
				want? <span class="pull-right" id="slide-submenu"> <i
					class="fa fa-times"></i>
			</span>
			</span> <a href="<c:url value="/clients/search"/>" class="list-group-item">
				<i class="fa fa-search"></i> Search clients
			</a>
		</div>
	</div>
	<div class="col-sm-8 col-md-9 sidebar">

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Document</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${clientsList}" var="client">
					<tr>
						<td><c:out value="${client.getName()}" /></td>
						<td><c:out value="${client.getDocument()}" /></td>
						<c:url value="/clients/edit" var="editURL">
							<c:param name="id" value="${client.getID()}" />
						</c:url>
						<c:url value="/clients/delete" var="deleteURL">
							<c:param name="id" value="${client.getID()}" />
						</c:url>
						<c:url value="/clients/client" var="officeURL">
							<c:param name="id" value="${client.getID()}" />
						</c:url>
						<td><a href="${editURL}">Edit</a></td>
						<td><a href="${deleteURL}">Delete</a></td>
						<td><a href="${officeURL}">Client's page</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>