<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="row">
	<div class="col-sm-4 col-md-3 sidebar">
		<div class="mini-submenu">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</div>
		<div class="list-group">
			<span href="#" class="list-group-item active"> What do you
				want? <span class="pull-right" id="slide-submenu"> <i
					class="fa fa-times"></i>
			</span>
			</span> </span> <a href="<c:url value="/clients"/>" class="list-group-item"> <i
				class="fa fa-arrow-left"></i> To clients list
			</a> <a href="<c:url value="/clients/search"/>" class="list-group-item">
				<i class="fa fa-search"></i> Search clients
			</a>
		</div>
	</div>

	<div class="col-sm-8 col-md-9 sidebar">
		Client ${client.getName()}'s page
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Document number</th>
					<th>Registration date</th>
					<th>Department link</th>
					<th>Type</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><c:out value="${client.getName()}" /></td>
					<td><c:out value="${client.getDocument()}" /></td>
					<td><c:out value="${client.getCreationDate()}" /></td>
					<c:url value="/offices/office" var="officeURL">
						<c:param name="id" value="${client.getOffice().getID()}" />
					</c:url>
					<td><a href="${officeURL}">Department's page</a></td>
					<td><c:choose>
							<c:when test="${client.getType() == true}">Corporate</c:when>
							<c:otherwise>Natural</c:otherwise>
						</c:choose></td>
					<c:url value="/clients/edit" var="editURL">
						<c:param name="id" value="${client.getID()}" />
					</c:url>
					<c:url value="/clients/delete" var="deleteURL">
						<c:param name="id" value="${client.getID()}" />
					</c:url>
					<td><a href="${editURL}">Edit</a></td>
					<td><a href="${deleteURL}">Delete</a></td>
				</tr>
			</tbody>
		</table>
		Following accounts belong to this client:

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Number</th>
					<th>Creation date</th>
					<th>Balance</th>
					<th>Payback date</th>
					<th>AccountType</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${accountsList}" var="account">
					<tr>
						<td><c:out value="${account.getNumber()}" /></td>
						<td><c:out value="${account.getCreationDate()}" /></td>
						<td><c:out value="${account.getBalance()}" /></td>
						<td><c:out value="${account.getPaybackDate()}" /></td>
						<td><c:url value="/accountTypes" var="accountTypesURL" /> <a
							href="${accountTypesURL}">AccountTypeID</a></td>

						<c:url value="/accounts/edit" var="editAccountURL">
							<c:param name="id" value="${account.getNumber()}" />
						</c:url>
						<c:url value="/accounts/delete" var="deleteAccountURL">
							<c:param name="id" value="${account.getNumber()}" />
						</c:url>
						<c:url value="/accounts/account" var="accountURL">
							<c:param name="id" value="${account.getNumber()}" />
						</c:url>
						<td><a href="${editAccountURL}">Edit</a></td>
						<td><a href="${deleteAccountURL}">Delete</a></td>
						<td><a href="${accountURL}">Account's page</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<c:url value="/accounts/add" var="addAccountURL">
			<c:param name="clientID" value="${client.getID()}" />
		</c:url>
		<a href="${addAccountURL}">Create a new account for this client</a> 
		
		</br>
		</br> </br> Address book of the client:

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Address</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${addressesList}" var="address">
					<tr>
						<td><c:out value="${address.getAddress()}" /></td>
						<c:url value="/addresses/edit" var="editAddressURL">
							<c:param name="address" value="${address.getAddress()}" />
						</c:url>
						<c:url value="/addresses/delete" var="deleteAddressURL">
							<c:param name="address" value="${address.getAddress()}" />
						</c:url>
						<td><a href="${editAddressURL}">Edit</a></td>
						<td><a href="${deleteAddressURL}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:url value="/addresses/add" var="addAddressURL">
			<c:param name="clientID" value="${client.getID()}" />
		</c:url>
		<a href="${addAddressURL}">Add a new address for this client</a>
		
		 </br>
		</br> </br> Client's emails:

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>E-mail</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${emailsList}" var="email">
					<tr>
						<td><c:out value="${email.getEmail()}" /></td>
						<c:url value="/emails/edit" var="editEmailURL">
							<c:param name="email" value="${email.getEmail()}" />
						</c:url>
						<c:url value="/emails/delete" var="deleteEmailURL">
							<c:param name="email" value="${email.getEmail()}" />
						</c:url>
						<td><a href="${editEmailURL}">Edit</a></td>
						<td><a href="${deleteEmailURL}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:url value="/emails/add" var="addEmailURL">
			<c:param name="clientID" value="${client.getID()}" />
		</c:url>
		<a href="${addEmailURL}">Add a new email for this client</a>
		
				 </br>
		</br> </br> Client's phones:

		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Phone</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${phonesList}" var="phone">
					<tr>
						<td><c:out value="${phone.getPhone()}" /></td>
						<c:url value="/phones/edit" var="editPhoneURL">
							<c:param name="phone" value="${phone.getPhone()}" />
						</c:url>
						<c:url value="/phones/delete" var="deletePhoneURL">
							<c:param name="phone" value="${phone.getPhone()}" />
						</c:url>
						<td><a href="${editPhoneURL}">Edit</a></td>
						<td><a href="${deletePhoneURL}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:url value="/phones/add" var="addPhoneURL">
			<c:param name="clientID" value="${client.getID()}" />
		</c:url>
		<a href="${addPhoneURL}">Add a new phone for this client</a>
	</div>
</div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>