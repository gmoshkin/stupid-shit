<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="row">
	<div class="col-sm-4 col-md-3 sidebar">
		<div class="mini-submenu">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</div>
		<div class="list-group">
			<span href="#" class="list-group-item active"> What do you
				want? <span class="pull-right" id="slide-submenu"> <i
					class="fa fa-times"></i>
			</span>
			</span><a href="<c:url value="/offices"/>" class="list-group-item"> <i
				class="fa fa-arrow-left"></i> To departments list
			</a> <a href="<c:url value="/offices/add"/>" class="list-group-item">
				<i class="fa fa-plus"></i> Add a department
			</a> <a href="<c:url value="/offices/search"/>" class="list-group-item">
				<i class="fa fa-search"></i> Search departments
			</a>
		</div>
	</div>
	<div class="col-sm-8 col-md-9 sidebar">
		Office ${office.getName()}'s page
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Address</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><c:out value="${office.getName()}" /></td>
					<td><c:out value="${office.getAddress()}" /></td>
					<c:url value="/offices/edit" var="editURL">
						<c:param name="id" value="${office.getID()}" />
					</c:url>
					<c:url value="/offices/delete" var="deleteURL">
						<c:param name="id" value="${office.getID()}" />
					</c:url>
					<td><a href="${editURL}">Edit</a></td>
					<td><a href="${deleteURL}">Delete</a></td>
				</tr>
			</tbody>
		</table>

		Clients registered in this department:
		<table class="table sortable table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Document</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${clientsList}" var="client">
					<tr>
						<td><c:out value="${client.getName()}" /></td>
						<td><c:out value="${client.getDocument()}" /></td>
						<c:url value="/clients/edit" var="editURL">
							<c:param name="id" value="${client.getID()}" />
						</c:url>
						<c:url value="/clients/delete" var="deleteURL">
							<c:param name="id" value="${client.getID()}" />
						</c:url>
						<c:url value="/clients/client" var="officeURL">
							<c:param name="id" value="${client.getID()}" />
						</c:url>
						<td><a href="${editURL}">Edit</a></td>
						<td><a href="${deleteURL}">Delete</a></td>
						<td><a href="${officeURL}">Client's page</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:url value="/clients/add" var="addClientURL">
			<c:param name="officeID" value="${office.getID()}" />
		</c:url>
		<a href="${addClientURL}">Register a new client in this department</a>
	</div>
</div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>