<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/header.jsp"%>

<div class="row">
	<div class="col-sm-4 col-md-3 sidebar">
		<div class="mini-submenu">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</div>
		<div class="list-group">
			<span href="#" class="list-group-item active"> What do you
				want? <span class="pull-right" id="slide-submenu"> <i
					class="fa fa-times"></i>
			</span>
			</span> <a href="<c:url value="/offices"/>" class="list-group-item"> <i
				class="fa fa-arrow-left"></i> To departments list
			</a> <a href="<c:url value="/offices/search"/>" class="list-group-item"> <i class="fa fa-search"></i>
				Search departments
			</a>
		</div>
	</div>
	<div class="col-sm-8 col-md-9 sidebar">
		<c:url var="post_url" value="/offices/update" />
		<form:form id="updateForm" modelAttribute="office" method="POST"
			action="${post_url}" role="form">
			<div class="form-group">
				<form:label path="name">Name</form:label>
				<form:input class="form-control" value="${office.getName()}" placeholder="name" path="name" />
			</div>
			<div class="form-group">
				<form:label path="address">Address</form:label>
				<form:input class="form-control" value="${office.getAddress()}" placeholder="address" path="address" />
			</div>
				<form:input type="hidden" path="ID" value="${office.getID()}" /> 
			<input type="submit" class="btn btn-primary" value="Update" />
		</form:form>
	</div>
</div>
<%@ include file="/WEB-INF/jsp/footer.jsp"%>